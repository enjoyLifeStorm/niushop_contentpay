<?php
/**
 * ICourse.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksapi;
interface ICourse
{
    
    /**
     * 添加专业（分类）
     * @param unknown $subject_name
     * @param unknown $parent_id
     * @param number $sort
     */
    function addUpdateSubject($subject_data);
    
    /**
     * 删除该专业
     * @param unknown $subject_id
     */
    function delSubject($subject_id);
    
    /**
     * 获取该专业详情
     * @param unknown $subject_id
     * @param string $condition
     */
    function getSubjectInfo($subject_id, $condition = '', $field ='*');
    
    /**
     * 获取专业分页
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getSubjectList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*');
    
    /**
     * 添加/修改教师信息
     */
    function addUpdateTeacher($teacher_data);
    
    /**
     * 删除教师信息
     * @param unknown $uid
     * @param string $condition
     */
    function delTeacher($ids, $condition = '');
    
    /**
     * 获取教师信息
     * @param unknown $uid
     * @param string $condition
     */
    function getTeacherInfo($id, $condition = '', $field ='*');
   
    /**
     * 获取分页教师信息
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getTeacherList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*');
    
    /**
     * 添加/修改课程信息
     */
    function addUpdateCourse($course_data, $teacher_ids = '');
    
    /**
     * 修改课程排序
     * @param unknown $course_id
     * @param unknown $sort
     */
    function updateCourseSort($course_id, $sort);
    
    /**
     * 删除课程信息
     * @param unknown $course_id
     * @param string $condition
     */
    function delCourse($course_id, $condition = '');
    
    /**
     * 根据课程目录计算课程的对应数值
     * @param unknown $course_id
     */
    function setCourseSumKpoint($course_id);
    
    /**
     * 获取课程详细信息
     * @param unknown $course_id
     * @param string $condition
     */
    function getCourseInfo($course_id, $condition = '', $field ='*');
    
    /**
     * 获取课程分页信息
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getCourseList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*');
    
    /**
     * 添加修改课程目录内容
     */
    function addUpdateCourseKpoint($course_kpoint_data);
    
    /**
     * 删除课程目录内容
     * @param unknown $kpoint_id
     * @param string $condition
     */
    function delCourseKpoint($kpoint_id, $condition = '');
    
    /**
     * 获取课程目录信息
     * @param unknown $kpoint_id
     * @param string $condition
     * @param string $field
     */
    function getCourseKpointInfo($kpoint_id, $condition = '', $field = '*');
    
    /**
     * 获取课程目录列表
     * @param unknown $course_id
     * @param string $condition
     * @param string $field
     */
    function getCourseKpointList($course_id, $condition = '', $field = '*', $order = 'sort asc');
    
    /**
     * 添加课程和讲师的关系
     * @param unknown $course_id
     * @param unknown $teacher_id
     */
    function addCourseTeacher($course_id, $teacher_id);
    
    /**
     * 删除课程和讲师的关系
     * @param unknown $course_id
     * @param string $condition
     */
    function delCourseTeacher($course_id, $condition = '');
    
    /**
     * 获取课程和讲师的关系
     * @param unknown $course_id
     * @param string $condition
     */
    function getCourseTeacher($course_id, $condition = '');
    
    /**
     * 根据讲师获取讲解的课程
     * @param unknown $teacher_id
     */
    function getTeacherByCourse($teacher_id);
    
    /**
     * 添加修改套餐
     * @param unknown $package_data
     * @param unknown $course_list
     */
    function addUpdatePackage($package_data);
    
    /**
     * 给套餐添加包含的课程
     * @param unknown $data
     */
    function addUpdatePackageCourse($data);
    
    /**
     * 批量添加套餐包含的课程
     * @param unknown $package_id
     * @param unknown $course_ids
     */
    function addBatchPackageCourse($package_id, $course_ids);
    
    /**
     * 获取套餐详情信息
     * @param unknown $package_id
     * @param string $condition
     * @param string $field
     */
    function getPackageInfo($package_id, $condition = '', $field = '*');
    
    /**
     * 获取套餐分页信息
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getPackageList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*');
    
    /**
     * 删除套餐
     * @param unknown $package_id
     * @param string $condition
     */
    function delPackage($package_ids, $condition = '');
    
    /**
     * 删除条件下套餐包含的课程信息
     * @param unknown $pc_id
     * @param string $condition
     */
    function delPackageCourse($pc_id, $condition = '');
    
    /**
     * 获取套餐包含的课程信息列表
     * @param unknown $package_id
     * @param string $condition
     */
    function getPackageCourseList($package_id, $condition = '');
    
    
}