<?php
/**
 * IVMemberAccount.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksapi;
interface IVmemberAccount
{
    /**
     * 添加修改会员学习记录
     */
    function addUpdateVmemberStudyHistory($kpoint_id, $play_stop_time);
    
    /**
     * 获取会员学习记录详情
     */
    function getVmemberStudyHistoryInfo($kpoint_id, $field = '*');
    
    /**
     * 添加评论及其回复
     * @param unknown $relation_type
     * @param unknown $relation_id
     * @param unknown $evaluate_text
     * @param unknown $reply_evaluate
     */
    function addMemberEvaluate($relation_type, $relation_id, $evaluate_text, $reply_evaluate = 0);
    
    /**
     * 根据被回复的评论id计算回复数量
     * @param unknown $evaluate_id
     */
    function setMemberEvaluateReplyCount($evaluate_id);
    
    /**
     * 查看评论及其回复信息信息
     * @param unknown $relation_type
     * @param unknown $relation_id
     * @param string $condition
     */
    function getMemberEvaluateList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*');
    
    /**
     * 添加点赞记录
     * @param unknown $type
     * @param unknown $relation_id
     */
    function addMemberDianzanRecord($type, $relation_id);
    
    /**
     * 计算会员点赞各项总数
     * @param unknown $type
     * @param unknown $relation_id
     * @param string $condition
     */
    function setMemberDianzanCount($type, $relation_id);
    
    /**
     * 添加收藏
     * @param unknown $type
     * @param unknown $relation_id
     * @param number $collection_label
     */
    function addMemberCollection($type, $relation_id, $label_id = 0);
    
    /**
     * 获取会员收藏记录
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getMemberCollectionList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*');
    
    /**
     * 删除会员收藏 
     * @param unknown $type 1课程 2套餐
     * @param unknown $relation_id
     */
    function delMemberCollection($type, $relation_id);
    
    /**
     * 计算收藏数量
     * @param unknown $type
     * @param unknown $relation_id
     */
    function setCollectionCount($type, $relation_id);
    /**
     * 获取该课程是否已收藏（自己是否已收藏）
     * @param unknown $relation_id
     * @param int $type
     * @param string $field
     */
    function isCourseCollection($relation_id , $type , $field = '*');
    
    /**
     * 会员添加学习笔记
     * @param unknown $kpoint_id
     * @param unknown $note_text
     * @param unknown $works_play_time
     */
    function addUpdateMemberStudyNote($note_id, $course_id, $course_title, $kpoint_id, $note_text, $works_play_time);
    
    /**
     * 删除学习笔记
     * @param unknown $note_id
     */
    function delMemberStudyNote($note_id);
    
    /**
     * 会员学习笔记详情
     * @param unknown $note_id
     */
    function getMemberStudyNoteInfo($note_id);
    
    /**
     * 会员学习作品笔记详情
     * @param unknown $works_code
     */
    function getMemberStudyWorksNoteInfo($works_code);
    
    /**
     * 会员学习笔记列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getMemberStudyNoteList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*');
    
    /**
     * 会员根据本章节获取学习笔记列表
     * @param unknown $kpoint_id
     */
    function getMemberKpointStudyNoteList($kpoint_id);
    
    /**
     * 添加课程讨论
     * @param unknown $course_id
     * @param unknown $kpoint_id
     * @param unknown $discuss_text
     */
    function addMemberCourseDiscuss($course_id, $kpoint_id, $discuss_text);
    
    /**
     * 获取课程评论列表
     * @param unknown $course_id
     */
    function getMemberCourseDiscussList($course_id);
    
    /**
     * 获取会员针对该课程最新播放目录(未传入目录id就按上次最新的播放地址走，如果未播放过取第一章第一节)
     * @param unknown $course_id
     */
    function getMemberCourseDefaultPlayKpoint($course_id);
    
    /**
     * 会员优惠劵领取
     * @param unknown vc_id 优惠劵id
     */
    function addMemberCoupon($vc_id);
    
    /**
     * 会员优惠劵删除
     * @param unknown $mc_id
     */
    function delMemberCoupon($mc_id);
    
    /**
     * 优惠劵使用
     * @param unknown $mc_id
     */
    function setMemberCouponUse($mc_id);
    
    /**
     * 会员优惠劵详情
     * @param unknown $mc_id
     */
    function getMemberCouponInfo($mc_id);
    
    /**
     * 根据优惠券id获取该会员是否领取该优惠券
     * @param unknown $coupon_id
     */
    function getMemberCouponByCouponid($coupon_id);
    
    /**
     * 会员优惠劵列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getMemberCouponList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*');
    
    /**
     * 获取会员拥有优惠劵总数量
     * @param string $condition
     */
    function getMemberCouponCount($condition = '');
    
    /**
     * 优惠劵退还
     * @param unknown $mc_id
     */
    function setMemberCouponReturn($mc_id);
}