<?php
/**
 * IOrder.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksapi;
interface IOrder
{
    /**
     * 创建视频订单
     * @param unknown $order_data
     * @param unknown $order_relation_list
     */
    function createOrder($order_data, $order_relation_list);
    
    /**
     * 创建视频订单项
     */
    function addOrderItem($v_order_id, $order_relation_list);
    
    /**
     * 获取订单详情
     * @param unknown $order_id
     * @param string $condition
     */
    function getOrderInfo($order_id, $condition = '', $field = '*');
    
    /**
     * 获取订单分页信息
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getOrderList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*');
    
    /**
     * 获取订单项列表
     * @param unknown $order_id
     * @param string $condition
     */
    function getOrderItemList($order_id, $condition = '');
    
    /**
     * 获取订单项详情
     * @param unknown $order_item_id
     * @param string $condition
     */
    function getOrderItemInfo($order_item_id, $condition = '');
    
    /**
     * 线上支付
     * @param unknown $out_trade_no
     * @param unknown $pay_type
     */
    function orderOnLinePay($out_trade_no, $pay_type);
    
    /**
     * 线下支付
     * @param unknown $out_trade_no
     */
    function orderLinePay($out_trade_no);
    
    /**
     * 订单完成
     * @param unknown $order_id
     */
    function orderComplete($order_id);
    
    /**
     * 订单项完成
     * @param unknown $order_item
     */
    function orderItemComplete($order_item);
    
    /**
     * 订单项销量计算
     * @param unknown $order_id
     */
    function setOrderItemBuyCount($order_id);
    
    /**
     * 计算同类型同关联id的订单完成数量
     * @param unknown $item_type
     * @param unknown $relation_id
     */
    function getOrderItemCount($item_type, $relation_id);
    
    /**
     * 根据传入的关联订单数据查询总金额
     * @param unknown $order_relation_list
     */
    function getOrderRelationListMoney($order_relation_list);
    
    /**
     * 根据传入的关联订单数据查询订单项
     * @param unknown $order_relation_list
     */
    function getOrderRelationItemList($order_relation_list);
    
    /**
     * 根据优惠劵和订单价格计算订单金额
     * @param unknown $mc_id
     * @param unknown $order_money
     */
    function setOrderCouponReplaceMoney($mc_id, $order_money);
    
    /**
     * 取消订单
     * @param unknown $v_order_id
     */
    function setOrderStatusCancel($v_order_id);
    /**
     * 获取订单数（图标数据）
     * @param unknown $condition
     */
    function getOrderCount($condition);
}