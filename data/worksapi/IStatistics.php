<?php
/**
 * IStatistics.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksapi;
/**
 * 统计
 * @author lzw
 *
 */
interface IStatistics
{
    /**
     * 获取时间区间状态订单涉及的金额
     * @param number $order_status
     * @param string $start_time
     * @param string $end_time
     */
    public function getOrderBuyTime($order_status = 2, $start_time = 0, $end_time = 0);
    
    /**
     * 获取时间区间内课程的发布数量
     * @param string $start_time
     * @param string $end_time
     */
    public function getCourseAddTime($start_time = 0, $end_time = 0);
    
    /**
     * 获取时间区间内套餐的发布数量
     * @param string $start_time
     * @param string $end_time
     */
    public function getPackageAddTime($start_time = 0, $end_time = 0);
    
    /**
     * 时间区间内排名课程所售获得的金额
     * @param string $start_time
     * @param string $end_time
     */
    public function getCourseListBuyPrice($start_time = 0, $end_time = 0);
    
    /**
     * 时间区间内各套餐所售获得的金额
     * @param string $start_time
     * @param string $end_time
     */
    public function getPackageListBuyPrice($start_time = 0, $end_time = 0);
    
    /**
     * 各时间订单涉及的金额 总金额 当年 当月 当周 当日
     */
    public function getOrderTimeByPrice();
    
    /**
     * 课程销售排行
     */
    public function getCourseSaleRanking($top = 10);
    
    /**
     * 各状态订单的数量
     */
    public function getOrderStatusCount();
    
    /**
     * 订单各时间销售数量
     */
    public function getOrderTimeSaleCount();
    /**
     * 不同状态下课程发布的数量
     */
    public function courseCount();
}