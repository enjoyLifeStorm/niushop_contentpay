<?php
/**
 * IWorks.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksapi;
interface IWorks
{
    
    /**
     * 添加修改作品库分类
     * @param unknown $db_class_data
     */
    function addUpdateDbClass($db_class_data);
    
    /**
     * 修改为默认作品库
     * @param unknown $class_id
     * @param string $condition
     */
    function setDbClassDefault($class_id, $condition = '');
    
    /**
     * 删除作品库分类
     * @param unknown $class_id
     * @param string $condition
     */
    function delDbClass($class_id, $condition = '');
    
    /**
     * 获取作品库分类详情
     * @param unknown $class_id
     * @param string $condition
     * @param string $filed
     */
    function getDbClassInfo($class_id, $condition = '', $filed = '*');
    
    /**
     * 获取作品库分类列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getDbClassList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*');
    
    /**
     * 添加修改作品库作品
     * @param unknown $db_works_data
     */
    function addUpdateDbWorks($db_works_data);
    
    /**
     * 删除作品库作品
     * @param unknown $works_id
     * @param string $condition
     */
    function delDbWorks($works_id, $condition = '');
    
    /**
     * 获取作品库作品详情
     * @param unknown $works_id
     * @param string $condition
     * @param string $filed
     */
    function getDbWorksInfo($works_id, $condition = '', $filed = '*');
    
    /**
     * 获取作品库作品列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getDbWorksList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*');
    
    /**
     * 获取指定分类下的作品数量
     * @param unknown $class_id
     * @param string $condition
     */
    function getDbClassWorksCount($class_id, $condition = '');
}