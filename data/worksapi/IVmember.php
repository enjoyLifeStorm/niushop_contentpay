<?php
/**
 * IVmember.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksapi;
interface IVmember
{
    /**
     * 添加修改视频会员信息
     * @param unknown $v_member_data
     */
    function addUpdateVmember($v_member_data);
    
    /**
     * 获取视频会员详情
     * @param unknown $uid
     * @param string $condition
     */
    function getVmemberInfo($uid, $condition = '', $field = '*');
    
    /**
     * 获取视频会员列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getVmemberList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*');
    
    /**
     * 添加会员拥有的课程、套餐
     * @param unknown $v_member_have_data
     */
    function addVmemberHave($v_member_have_data);
    
    /**
     * 根据订单发放会员拥有
     * @param unknown $order_id
     */
    function addVmemberOrderByHave($order_id);
    
    /**
     * 获取会员拥有的课程、套餐详情信息
     * @param unknown $have_id
     * @param string $condition
     */
    function getVmemberHaveInfo($have_id, $condition = '');
    
    /**
     * 获取会员拥有的课程、套餐列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getVmemberHaveList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*');
    
    /**
     * 计算课程学习进度百分比
     * @param unknown $course_id
     */
    //function setVmemberHaveStudyRate($course_id);
    
    /**
     * 添加修改会员等级信息
     * @param unknown $v_member_level_data
     */
    function addUpdateVmemberLevel($v_member_level_data);
    
    /**
     * 删除会员等级信息
     * @param unknown $v_level_id
     * @param string $condition
     */
    function delVmemberLevel($v_level_id, $condition = '');
    
    /**
     * 获取会员等级详情
     * @param unknown $v_level_id
     * @param string $condition
     */
    function getVmemberLevelInfo($v_level_id, $condition = '');
    
    /**
     * 获取会员等级列表
     * @param number $page_index
     * @param number $page_size
     * @param string $condition
     * @param string $order
     * @param string $field
     */
    function getVmemberLevelList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*');
    
    /**
     * 设置默认等级
     * @param unknown $level_id
     */
    function setVmemberLevelDefault($level_id);
    
    /**
     * 获取会员拥有课程详情
     * @param unknown $course_id
     */
    function getVmemberHaveIsCourse($relation_id , $relation_type );
    
}