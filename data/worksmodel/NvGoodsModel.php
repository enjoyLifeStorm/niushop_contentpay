<?php
/**
 * NvGoodsModel.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace data\worksmodel;

use data\model\BaseModel as BaseModel;
/**
 * 商品表
 */

class NvGoodsModel extends BaseModel {

    protected $table = 'nv_goods';
    protected $rule = [
        'goods_id'  =>  '',
        'goods_content'  =>  'no_html_parse',
    ];
    protected $msg = [
        'course_id'  =>  '',
        'goods_content'  =>  '',
    ];
    
    /**
     * 获取列表
     * @param unknown $page_index
     * @param unknown $page_size
     * @param unknown $condition
     * @param unknown $order  */
    public function getViewQuery($page_index, $page_size, $condition, $order){
        
        //设置查询视图
        $viewObj = $this->alias('ng')
        ->join('nv_goods_class ngc', 'ngc.class_id = ng.class_id')
        ->field('ng.goods_id,ng.goods_name,ng.goods_price,ng.goods_img,ng.goods_content,ngc.class_name');
        
        $list = $this->viewPageQuery($viewObj, $page_index, $page_size, $condition, $order);
        
        return $list;
        
    }
    
    /**
     * 获取统计数量
     * @param unknown $condition  */
    public function getViewCount($condition){
        //设置查询视图
        $viewObj = $this->alias('ng')
        ->join('nv_goods_class ngc', 'ngc.class_id = ng.class_id')
        ->field('ng.goods_id,ng.goods_name,ng.goods_price,ng.goods_img,ng.goods_content,ngc.class_name');
        
        $count = $this->viewCount($viewObj, $condition);
    }
    
    /**
     * 获取列表返回数据格式
     * @param unknown $page_index
     * @param unknown $page_size
     * @param unknown $condition
     * @param unknown $order  */
    public function getViewList($page_index, $page_size, $condition, $order){
        
        $list = $this->getViewQuery($page_index, $page_size, $condition, $order);
        $count = $this->getViewCount($condition);
        
        $return_list = $this->setReturnList($list, $count, $page_size);
        return $return_list;
        
    }
}