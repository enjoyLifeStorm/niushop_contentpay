<?php
/**
 * Statistics.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksservice;
use data\service\BaseService;
use data\worksservice\Order;
use data\worksapi\IStatistics;
use data\worksmodel\NvOrderModel;
use data\worksmodel\NvCourseModel;
use data\worksmodel\NvPackageModel;
use data\worksmodel\NvOrderItemModel;
use data\model\BaseModel;
use think\helper\Time;

/**
 * 统计
 * @author lzw
 *
 */
class Statistics extends BaseService implements IStatistics
{
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IStatistics::getOrderBuyTime()
     */
    public function getOrderBuyTime($order_status = 2, $start_time = 0, $end_time = 0){
       
        if(empty($end_time)) $end_time = time();
        $condition = array(
            'order_status'  => $order_status,
            'status_time'   => array('between time',[$start_time, $end_time])
        );
        $v_order_model = new NvOrderModel();
        $v_order_money_sum = $v_order_model->getSum($condition, 'order_money');
        return $v_order_money_sum;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IStatistics::getCourseAddTime()
     */
    public function getCourseAddTime($start_time = 0, $end_time = 0){
        
        if(empty($end_time)) $end_time = time();
        
        $condition['create_time'] = array('between time',[$start_time, $end_time]);
        $course_model = new NvCourseModel();
        $course_add_count = $course_model->getCount($condition);
        return $course_add_count;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IStatistics::getPackageAddTime()
     */
    public function getPackageAddTime($start_time = 0, $end_time = 0){
        
        if(empty($end_time)) $end_time = time();
        $condition['create_time'] = array('between time',[$start_time, $end_time]);
        $package_model = new NvPackageModel();
        $package_count = $package_model->getCount($condition);
        return $package_count;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IStatistics::getCourseListBuyPrice()
     */
    public function getCourseListBuyPrice($start_time = 0, $end_time = 0){
        
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IStatistics::getPackageListBuyPrice()
     */
    public function getPackageListBuyPrice($start_time = 0, $end_time = 0){
        
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IStatistics::getOrderTimeByPrice()
     */
    public function getOrderTimeByPrice(){
        
        $time_price_array = array();
        //总成交金额
        $time_price_array['sum_order_money'] = $this->getOrderBuyTime();
        
        //本年成交金额
        list($start, $end) = Time::year();
        $time_price_array['current_year_order_money'] = $this->getOrderBuyTime(2, $start, $end);
        
        //当月成交金额
        list($start, $end) = Time::month();
        $time_price_array['current_month_order_money'] = $this->getOrderBuyTime(2, $start, $end);
        
        //本周成交金额
        list($start, $end) = Time::week();
        $time_price_array['current_week_order_money'] = $this->getOrderBuyTime(2, $start, $end);
        
        //今天成交金额
        list($start, $end) = Time::today();
        $time_price_array['current_day_order_money'] = $this->getOrderBuyTime(2, $start, $end);
        
        return $time_price_array;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IStatistics::getCourseSaleRanking()
     */
    public function getCourseSaleRanking($top = 10){
        
        $course_model = new NvCourseModel();
        $course_list = $course_model->where('1=1')->order(['buy_count'=>'desc'])->limit($top)->select();
        
        return $course_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IStatistics::getOrderStatusCount()
     */
    public function getOrderStatusCount(){
        
        $order_status_arr = array();
        $vorder_model = new NvOrderModel();
        
        //总下单数量
        $order_status_arr['create'] = $vorder_model->getCount(['order_status'=>0]);
        
        //完成订单数量
        $order_status_arr['finish'] = $vorder_model->getCount(['order_status'=>2]);
        
        //取消订单数量
        $order_status_arr['cancel'] = $vorder_model->getCount(['order_status'=>-1]);
        
        return $order_status_arr;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IStatistics::getOrderTimeSaleCount()
     */
    public function getOrderTimeSaleCount(){
        
        $sale_count_arr = array();
        
        //总成交数量
        $sale_count_arr['order_count'] = $this->getOrderBuyTimeSale();
        
        //本年成交数量
        list($start, $end) = Time::year();
        $sale_count_arr['current_year_order_count'] = $this->getOrderBuyTimeSale(2, $start, $end);
        
        //当月成交数量
        list($start, $end) = Time::month();
        $sale_count_arr['current_month_order_count'] = $this->getOrderBuyTimeSale(2, $start, $end);
        
        //本周成交数量
        list($start, $end) = Time::week();
        $sale_count_arr['current_week_order_count'] = $this->getOrderBuyTimeSale(2, $start, $end);
        
        //今天成交数量
        list($start, $end) = Time::today();
        $sale_count_arr['current_day_order_count'] = $this->getOrderBuyTimeSale(2, $start, $end);
        
        return $sale_count_arr;
    }
    
    /**
     * 指定状态销售数量
     * @param number $order_status
     * @param number $start_time
     * @param number $end_time
     * @return \data\model\unknown
     */
    public function getOrderBuyTimeSale($order_status = 2, $start_time = 0, $end_time = 0){
         
        if(empty($end_time)) $end_time = time();
        $condition = array(
            'order_status'  => $order_status,
            'status_time'   => array('between time',[$start_time, $end_time])
        );
        $v_order_model = new NvOrderModel();
        $v_order_count = $v_order_model->getCount($condition);
        return $v_order_count;
    }
    /**
     * 不同状态下课程发布的数量
     */
    public function courseCount(){
        $course_status_arr = array();
        $vcourse_model = new NvCourseModel();
        //已发布数量
        $course_status_arr['published'] = $vcourse_model->getCount(['status'=>2]);
        //草稿数量
        $course_status_arr['draft'] = $vcourse_model->getCount(['status'=>0]);
        return $course_status_arr;
    }
    
    
    
    public function getcourseSalesList($page_index = 1, $page_size = 0, $condition = '', $order = '')
    {
        // $goods_calculate = new GoodsCalculate();
        // $goods_sales_list = $goods_calculate->getGoodsSalesInfoList($page_index, $page_size , $condition , $order );
        // return $goods_sales_list;
        $course_model = new NvCourseModel();
        $tmp_array = $condition;
//         if (! empty($condition["status"])) {
//             $order_condition["status"] = $condition["status"];
//             unset($tmp_array["status"]);
//         }
        $course_list = $course_model->pageQuery($page_index, $page_size, $tmp_array, $order, '*');
        
        foreach($course_list['data'] as $item){
            
            // 获取7天前的时间戳
            $day_time = Time::daysAgo(30);
            $condition = array(
                'item_type' => 1,
                'relation_id' => $item['course_id'],
                'status_time' => array('gt', $day_time)
            );
            
            $v_order_item_model = new NvOrderItemModel();
            $item['finish_count'] = $v_order_item_model->getCount($condition);
            $item['sum_order_money'] = $v_order_item_model->getSum($condition, 'price'); 
            
        }
  
        return $course_list;
    }
    
    
    
    
}