<?php
/**
 * Vmember.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksservice;
use data\service\BaseService;
use data\worksmodel\NvMemberModel;
use data\worksmodel\NvMemberLevelModel;
use data\worksmodel\NvMemberHaveModel;
use think\Log;
class Vmember extends BaseService implements \data\worksapi\IVmember
{
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IVmember::addUpdateVmember()
     */
    public function addUpdateVmember($v_member_data){
        
        if(empty($v_member_data['uid'])) return false;
        
        $v_member_info = $this->getVmemberInfo($v_member_data['uid']);
        
        $v_member_model = new NvMemberModel();

        if(empty($v_member_info)){
            
            $v_default_member_info = $this->getVmemberLevelInfo(0, ['is_default' => 1]);
            $v_member_data['level_id'] = $v_default_member_info['level_id'];
            $v_member_model->save($v_member_data);
        }else{
            
            $v_member_model->save($v_member_data, ['uid' => $v_member_data['uid']]);
        }
        return $v_member_data['uid'];
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IVmember::getVmemberInfo()
     */
    public function getVmemberInfo($uid, $condition = '', $field = '*'){
        
        if(!empty($uid)) $condition['uid'] = $uid;
        if(empty($condition)) return false;
        
        $v_member_model = new NvMemberModel();
        $v_member_info = $v_member_model->getInfo($condition, $field);
        return $v_member_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IVmember::getVmemberList()
     */
    public function getVmemberList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field ='*'){
        
        $v_member_model = new NvMemberModel();
        $v_member_list = $v_member_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        return $v_member_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IVmember::addUpdateVmemberLevel()
     */
    public function addUpdateVmemberLevel($v_member_level_data){
        
        $v_member_level_model = new NvMemberLevelModel();
        
        $level_id = $v_member_level_data['level_id'];
        if(empty($level_id)){
            
            $level_id = $v_member_level_model->save($v_member_level_data);
        }else{
            
            $v_member_level_model->save($v_member_level_data, ['level_id' => $level_id]);
        }
        
        //如果当前的等级设置为默认就将其他默认修改为非
        if(!empty($v_member_level_data['is_default'])){
            
            $this->setVmemberLevelDefault($level_id);
        }
        
        return 1;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IVmember::setVmemberLevelDefault()
     */
    public function setVmemberLevelDefault($level_id){
        
        $v_member_level_model = new NvMemberLevelModel();
        $v_member_level_model->save(['is_default' => 0], '1=1');
        $v_member_level_model->save(['is_default' => 0], ['level_id' => $level_id]);
        
        return 1;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IVmember::delVmemberLevel()
     */
    public function delVmemberLevel($v_level_id, $condition = '*'){
        
        if(!empty($v_level_id)) $condition['level_id'] = $v_level_id;
        
        $v_member_level_model = new NvMemberLevelModel();
        $res = $v_member_level_model->destroy($condition);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IVmember::getVmemberLevelInfo()
     */
    public function getVmemberLevelInfo($v_level_id, $condition = ''){
        
        if(!empty($v_level_id)) $condition['level_id'] = $v_level_id;
        if(empty($condition)) return false;
        
        $v_member_level_model = new NvMemberLevelModel();
        $member_level_info = $v_member_level_model->getInfo($condition, '*');
        return $member_level_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IVmember::getVmemberLevelList()
     */
    public function getVmemberLevelList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
        
        $v_member_level_model = new NvMemberLevelModel();
        $v_member_level_list = $v_member_level_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        return $v_member_level_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IVmember::addVmemberHave()
     */
    public function addVmemberHave($v_member_have_data){
        
        $condition['uid'] = $v_member_have_data['uid'];
        $condition['relation_type'] = $v_member_have_data['relation_type'];
        $condition['relation_id'] = $v_member_have_data['relation_id'];
        
        $v_member_have_info = $this->getVmemberHaveInfo(0, $condition);
        $v_member_have_model = new NvMemberHaveModel();
        
        if(empty($v_member_have_info)){
            
            //此处包含了两种情况 1.首次购买课程 2.首次购买章节
            $res = $v_member_have_model->save($v_member_have_data);
            return $res;
        }else{
            
            // 如果搜到了也有两种情况 1.购买过该课程  2.购买过该课程中的章节
            if(empty($v_member_have_info['kpoint_id'])){
                
                //第一种情况 又包含两种情况 再购买课程 再购买章节 应该都不会出现 前面会做阻挡 
                return NV_USER_HAVE_EXIST;
            }else{
                //第二种情况 同样包含两种情况 再购买课程 再购买章节
                if(empty($v_member_have_data['kpoint_id'])){
                    
                    $res = $v_member_have_model->save(['kpoint_id'=>''], ['have_id' => $v_member_have_info['have_id']]);
                    return $res;
                }else{
                    $kpoint_arr = explode(',', $v_member_have_info['kpoint_id']);
                    if(in_array($v_member_have_data['kpoint_id'], $kpoint_arr)){
                    
                        // 再购买章节之前已有的情况  此种情况应该是不会出现的 因为在前面会做判断 已拥有的章节就不会出现购买按钮
                        return NV_USER_HAVE_EXIST;
                    }else{
                        // 再购买章节之前没有的情况
                        $data['kpoint_id'] = $v_member_have_info['kpoint_id'].','.$v_member_have_data['kpoint_id'];
                        $res = $v_member_have_model->save($data, ['have_id' => $v_member_have_info['have_id']]);
                        return $res;
                    }
                }
                
            }
             
        }
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IVmember::getVmemberHaveInfo()
     */
    public function getVmemberHaveInfo($have_id, $condition = ''){
     
        if(!empty($have_id)) $condition['have_id'] = $have_id;
        
        $v_member_have_model = new NvMemberHaveModel();
        $v_member_have_info = $v_member_have_model->getInfo($condition, '*');
        return $v_member_have_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IVmember::getVmemberHaveList()
     */
    public function getVmemberHaveList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
        
        $condition['uid'] = $this->uid;
        
        $v_member_have_model = new NvMemberHaveModel();
        $v_member_have_list = $v_member_have_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        
        //获取各关联类型下的信息 1课程 2套餐
        foreach ($v_member_have_list['data'] as $item){
            
            $course_service = new Course();
            if($item['relation_type'] == 1){
                
                $item['course_info'] = $course_service->getCourseInfo($item['relation_id'],'','course_id,course_title,lession_num,time_length,cover_img,course_dec,status');
            }elseif($item['relation_type'] == 2){
                
                $item['package_info'] = $course_service->getPackageInfo($item['relation_id'],'','package_id,package_title,cover_img,package_dec,end_time,lose_type,lose_time');
            }
            
            switch ($item['status']) {
                case 0:
                    $item['status_name'] = '未学习';
                    break;
                case 1:
                    $item['status_name'] = '学习中';
                    break;
                case 2:
                    $item['status_name'] = '学习完成';
                    break;
                case -1:
                    $item['status_name'] = '已过期';
                    break;
                default:break;
            }
            
            if($item['end_time'] == 0){
                
                $item['end_time'] = '永久';
            }else{
                
                $item['end_time'] = getTimeStampTurnTime($item['end_time'],'Y-m-d');
            }
            
        }
        return $v_member_have_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmember::addVmemberOrderByHave()
     */
    public function addVmemberOrderByHave($order_id){
       
        $member_have_model = new NvMemberHaveModel();
        $member_have_model->startTrans();
        try {
            $v_order_service = new Order();
            $order_item_list = $v_order_service->getOrderItemList($order_id);
            
            $course_service = new Course();
            foreach($order_item_list as $item){
            
                $data = array(
                    'uid'           => $item['uid'],
                    'relation_type' => $item['item_type'],
                    'relation_id'   => $item['relation_id'],
                    'create_time'   => time(),
                    'status_time'   => time()
                );
            
                if($item['item_type'] == 1){  //如果订单项是课程
                    
                    $course_info = $course_service->getCourseInfo($item['relation_id']);
                    if($course_info['lose_type'] == 1){
            
                        //如果为0说明无过期日期
                        if($course_info['lose_time'] == 0){
                        
                            $data['end_time'] = 0;
                        }else{
                            $data['end_time'] = time() + ($course_info['lose_time'] * 24 * 60 * 60);
                        }
                       
                    }else{
                        
                        $data['end_time'] = $course_info['end_time'];
                    }
                    
                    if(!empty($item['kpoint_id'])){
                        $data['kpoint_id'] = $item['kpoint_id'];
                    }
                    
                }elseif ($item['item_type'] == 2){  //如果订单项是套餐
            
                    $package_info = $course_service->getPackageInfo($item['relation_id']);
            
                    if($package_info['lose_type'] == 0){
                        
                        $data['end_time'] = $package_info['end_time'];
                    }elseif($package_info['lose_type'] == 1){
                        
                        //如果为0说明无过期日期
                        if($package_info['lose_time'] == 0){
                            
                            $data['end_time'] = 0;
                        }else {
                            
                            $data['end_time'] = time() + ($package_info['lose_time'] * 24 * 60 * 60);
                        }
                        
                    }
            
                }
            
                $have_id = $this->addVmemberHave($data);
            
                //如果是套餐添加成功后需要再添加其子项包含课程
                if($item['item_type'] == 2){
            
                    $package_course_list = $course_service->getPackageCourseList($item['relation_id']);
            
                    foreach($package_course_list as $course_item){
            
                        $have_course_data = array(
                            'uid'           => $item['uid'],
                            'relation_type' => 1,
                            'relation_id'   => $course_item['course_id'],
                            'create_time'   => time(),
                            'status_time'   => time(),
                            'parent_id'     => $have_id,
                            'end_time'      => $data['end_time']
                        );
                        $this->addVmemberHave($have_course_data);
                    }
            
                }
            }
            
            $member_have_model->commit();
            return 1;
        } catch (\Exception $e) {
            
            $member_have_model->rollback();
            Log::write('订单支付成功给会员添加拥有项报错，错误为：' . $e->getMessage());
            return $e->getMessage();
        }
        
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmember::getVmemberHaveIsCourse()
     */
    public function getVmemberHaveIsCourse($relation_id , $relation_type=1){
       
        //获取课程价格如果课程为免费会员不需要拥有
        $course_service = new Course();
        if($relation_type == 1){
            
            $course_price = $course_service->getCourseInfo($relation_id,'','current_price')['current_price'];
            if($course_price == 0.00){
                return 1;
            }
        }
        
        
        $condition = array(
            'uid'   => $this->uid,
            'relation_type' => $relation_type,
            'relation_id'   => $relation_id,
            'end_time'      => array(array('gt', time()),array('eq',0),'or')
        );
        
        $v_member_have_model = new NvMemberHaveModel();
        $member_have_course = $v_member_have_model->getInfo($condition, 'have_id');
        
        return empty($member_have_course) ? 0 : 1;
    }
    
}