<?php
/**
 * Order.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksservice;
use data\worksservice\Order as orderService;
use data\worksmodel\NvOrderModel;
use data\model\UserModel;
use data\worksmodel\NvOrderItemModel;
use data\service\BaseService;
use data\service\UnifyPay;
use data\worksmodel\NvCourseModel;
use data\worksmodel\NvPackageModel;
use think\Log;
use data\service\Member;
use data\service\Member\MemberAccount;
use data\service\Order\OrderStatus;
class Order extends BaseService implements \data\worksapi\IOrder
{
    
    /**
     * (non-PHPdoc)
     * @see \data\video\api\IOrder::createOrder()
     */
    public function createOrder($order_data, $order_relation_list){
        
        
        $v_order_model = new NvOrderModel();
        $v_order_model->startTrans();
        $type = json_decode($order_relation_list , true)[0]['relation_type'];
        
        try {
            
            //订单总金额
            $sum_order_money = $this->getOrderRelationListMoney($order_relation_list);
            
            //减了优惠劵后的金额
            $order_money = $this->setOrderCouponReplaceMoney($order_data['mc_id'], $sum_order_money);
            
            //如果优惠劵不为空那就算计优惠劵顶替的金额
            if(!empty($order_data['mc_id'])){
                
                $order_data['coupon_money'] = $sum_order_money - $order_money;
                
                //使用优惠劵
                $member_account_service = new VmemberAccount();
                $member_account_service->setMemberCouponUse($order_data['mc_id']);
            }
            
            
            $order_data['order_money'] = $sum_order_money;
            $order_data['order_type'] = 1;
            //实际支付金额
//             $pay_money = $order_money - $order_data['balance'] - $order_data['point_money'];
            if($order_money > $order_data['balance']){
                
                $pay_money = $order_money - $order_data['balance'];
            }else{
                $order_data['balance'] = $order_money;
                $pay_money = 0.00;
            }
            
            $order_data['pay_money'] = $pay_money;
            
            //赠送的积分
//             $order_data['give_point'] = $course_service->getCourseListSumGivePoint($course_list);
            
            //填充基本信息
            $order_data['order_no'] = $this->createOrderNo();
            $out_trade_no = $this->createOutTradeNo();
            $order_data['out_trade_no'] = $out_trade_no;
            $order_data['uid']  = $this->uid;
            
            // 获取购买人信息
            $buyer = new UserModel();
            $buyer_info = $buyer->getInfo([
                'uid' => $this->uid
            ], 'nick_name');
            
            $order_data['user_name'] = $buyer_info['nick_name'];
            $order_data['create_time'] = time();
            $order_data['status_time'] = time();
            
            // 订单来源
            if (isWeixin()) {
                $order_from = 1; // 微信
            } elseif (request()->isMobile()) {
                $order_from = 2; // 手机
            } else {
                $order_from = 3; // 电脑
            }
            $order_data['order_from'] = $order_from;
            
            $v_order_id = $v_order_model->save($order_data);
            
            //添加订单项
            $this->addOrderItem($v_order_id, $order_relation_list);
            

            //添加支付流水
            $unify_pay_service = new UnifyPay();
            if($type == 1){
                
                $unify_pay_service->createPayment(0, $out_trade_no, '课程购买', '课程购买', $pay_money, 3, $v_order_id);
            }else{
                
                $unify_pay_service->createPayment(0, $out_trade_no, '套餐购买', '套餐购买', $pay_money, 3, $v_order_id);
            }
           
            
            //使用了积分或者以后执行
            if(!empty($order_data['balance']) || !empty($order_data['point'])){
                
                $member_service = new Member();
                $account_info = $member_service->getMemberAccount($this->uid, $this->instance_id);
                
                //余额不足
                if($order_data['balance'] > $account_info['balance']){
                    
                    $v_order_model->rollback();
                    return NV_USER_BALANE_NO_FOOT;
                }else{
                    
                    $account_flow = new MemberAccount();
                    $account_flow->addMemberAccountData(0, 2, $this->uid, 0, $order_data['balance'] * (- 1), 21, $v_order_id, '课程购买');
                }
                
                //积分不足
                if($order_data['point'] > $account_info['point']){
                    
                    $v_order_model->rollback();
                    return NV_USER_POINT_NO_FOOT;
                }
            }
            
            //支付金额为空就不需要支付
//             if($pay_money == 0.00){
               
//                 return NV_ORDER_ALREADY_PAYMENT;
//             }
            
            $v_order_model->commit();
            return $out_trade_no;
        } catch (\Exception $e) {
            
            $v_order_model->rollback();
            Log::write('订单创建失败' . $e->getMessage());
            return $e->getMessage();
        }
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\video\api\IOrder::addOrderItem()
     */
    public function addOrderItem($v_order_id, $order_relation_list){

        $relation_arr = json_decode($order_relation_list, true);
        foreach ($relation_arr as $item){
            
            $condition = array(
                'item_type'   => $item['relation_type'],
                'relation_id' => $item['relation_id'],
                'kpoint_id'   => $item['kpoint_id'],
                'uid'         => $this->uid,
                'order_status'=> 0
            );
            $order_item_info = $this->getOrderItemInfo(0, $condition);
            //如果已存在该课程订单直接将以前订单取消如有购物车需重新进行更改
            if(!empty($order_item_info)){
                
                $this->setOrderStatusCancel($order_item_info['v_order_id']);
            }
            
            $v_order_item_model = new NvOrderItemModel();
            
            $data = array(
                'v_order_id'  => $v_order_id,
                'item_type'   => $item['relation_type'],
                'relation_id' => $item['relation_id'],
                'kpoint_id'   => $item['kpoint_id'],
                'uid'         => $this->uid,
                'point_exchange_type'   => 1,
                'order_status'  => 0,
                'give_point'    => 0
            );
            
            if($item['relation_type'] == 1){
            
                $course_service = new Course();
                $course_info = $course_service->getCourseInfo($item['relation_id']);
                if(!empty($item['kpoint_id'])){
                    $kpoint_info = $course_service->getCourseKpointInfo($item['kpoint_id']);
                }
                
                $data['item_title'] = $course_info['course_title'];
                $data['price'] = $course_info['current_price'];
                if(!empty($item['kpoint_id'])){
                    $data['price'] = $kpoint_info['current_price'];
                }
                $data['cover_img'] = $course_info['cover_img'];
                
            }else if($item['relation_type'] == 2){
                
                $course_service = new Course();
                $package_info = $course_service->getPackageInfo($item['relation_id']);
                
                $data['item_title'] = $package_info['package_title'];
                $data['price'] = $package_info['current_price'];
                $data['cover_img'] = $package_info['cover_img'];
                
            }else if($item['relation_type'] == 3){
                
            }
            
            $v_order_item_model->save($data);
        }
        
        return 1;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\video\api\IOrder::getOrderInfo()
     */
    public function getOrderInfo($order_id, $condition = '', $field = '*'){
        
        if(!empty($order_id)) $condition['v_order_id'] = $order_id;
        if(empty($condition)) return false;
        
        $v_order_model = new NvOrderModel();
        $order_info = $v_order_model->getInfo($condition, $field);
        if($field == '*'){
            $order_info['pay_type_name'] = OrderStatus::getPayType($order_info['payment_type']);
        }
        return $order_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\video\api\IOrder::getOrderList()
     */
    public function getOrderList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
        
        $v_order_model = new NvOrderModel();
        $order_list = $v_order_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        
        foreach($order_list['data'] as $item){
            
            $item['item_list'] = $this->getOrderItemList($item['v_order_id']);
            
           
            $status_name = '';
            switch ($item['order_status']) {
                case -1:
                    $status_name = '已取消';
                    break;
                case 1:
                    $status_name = '已支付';
                    break;
                case 2:
                    $status_name = '已完成';
                    break;
                default:
                    $status_name = '待支付';
                    break;
            }
            $item['status_name'] = $status_name;
            
            // 订单来源名称
            $order_from = OrderStatus::getOrderFrom($item['order_from']);
            $item['order_from_name'] = $order_from['type_name'];
            $item['order_from_tag'] = $order_from['tag'];
            
            $item['create_time'] = getTimeStampTurnTime($item['create_time']);
        }
        
        return $order_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\video\api\IOrder::getOrderItemInfo()
     */
    public function getOrderItemInfo($order_item_id, $condition = ''){
        
        if(!empty($order_item_id)) $condition['order_item_id'] = $order_item_id;
        if(empty($condition)) return false;
        
        $v_order_item_model = new NvOrderItemModel();
        $order_item_info = $v_order_item_model->getInfo($condition, '*');
        return $order_item_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\video\api\IOrder::getOrderItemList()
     */
    public function getOrderItemList($order_id, $condition = ''){
        
        if(!empty($order_id)) $condition['v_order_id'] = $order_id;
        if(empty($condition)) return false;
        
        $v_order_item_model = new NvOrderItemModel();
        $course = new Course();
        $order_item_list = $v_order_item_model->getQuery($condition, '*', 'order_item_id asc');
        
        
        foreach($order_item_list as $item){
         
            $status_name = '';
            switch ($item['order_status']) {
                case 0:
                    $status_name = '待支付';
                    break;
                case 1:
                    $status_name = '已支付';
                    break;
                case 2:
                    $status_name = '已完成';
                    break;
                case -1:
                    $status_name = '已取消';
                    break;
                default:
                    $status_name = '待支付';
                break;
            }
            $item['status_name'] = $status_name;
            
            if(!empty($item['kpoint_id'])){
                $item['kpoint_name'] = $course->getCourseKpointInfo($item['kpoint_id'],'','kpoint_name')['kpoint_name'];
            }else{
                $item['kpoint_name'] = '';
            }
        }
        return $order_item_list;
    }
    
    
    /**
     * 一段时间内的课程销售量
     * @param unknown $condition
     */
    public function getCourseSalesNum($order_course_list, $course_id){
        $sales_num = 0;
        foreach( $order_course_list as $k=>$v){
            if($v["relation_id"] ==$course_id ){
                $sales_num = $sales_num + $v["num"];
            }
        }
        return $sales_num;
    }
    
    
  
    
    
    
    /**
     * 一段时间内的课程下单金额
     * @param unknown $condition
     */
    public function getCourseSalesMoney($order_course_list, $course_id){
        $sales_money = 0;
        foreach( $order_course_list as $k=>$v){
            if($v["relation_id"] ==$course_id ){
                $sales_money = $sales_money + ($v["price"] - $v["refund_require_money"]);
            }
        }
        return $sales_money;
    }
    
    
    
   
    /**
     * 查询一段时间内的店铺下单量
     *
     * @param unknown $shop_id
     * @param unknown $start_date
     * @param unknown $end_date
     * @return unknown
     */
    public function getShopSaleNumSum($condition)
    {
        $order_account = new orderService();
        $sales_num = $order_account->getShopSaleNumSum($condition);
        return $sales_num;
    }
    
   
    
    /**
     * 查询一段时间下单量
     * @param unknown $shop_id
     * @param unknown $start_date
     * @param unknown $end_date
     * @return unknown|number
     */
    public function getShopSaleSum($condition){
        $order_model = new NvOrderModel();
        $order_sum = $order_model->getSum($condition,'pay_money');
        if(!empty($order_sum))
        {
            return $order_sum;
        }else{
            return 0;
        }
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\video\api\IOrder::orderLinePay()
     */
    public function orderLinePay($out_trade_no){
        
        $v_order_model = new NvOrderModel();
        $v_order_model->startTrans();
        
        try {
            
            $order_info = $this->getOrderInfo(0, ['out_trade_no' => $out_trade_no]);
            
            //修改订单状态
            $v_order_model->save(['order_status' => 1,'payment_type' => 10,'pay_time'=>time(),'status_time'=>time()], ['out_trade_no' => $out_trade_no]);
            
            //修改订单项状态
            $v_order_item_model = new NvOrderItemModel();
            $v_order_item_model->save(['order_status' => 1], ['v_order_id' => $order_info['v_order_id']]);
            
            //支付成功发放拥有课程及其套餐
            $v_member_service = new Vmember();
            $v_member_service->addVmemberOrderByHave($order_info['v_order_id']);
            
            //初步只要支付就为完成状态
            $this->orderComplete($order_info['v_order_id']);
            
            $v_order_item_model->commit();
            return 1;
        } catch (\Exception $e) {
            
            $v_order_item_model->rollback();
            Log::write('订单线下支付成功后执行报错，错误：'. $e->getMessage());
            return $e->getMessage();
        }
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\video\api\IOrder::orderOnLinePay()
     */
    public function orderOnLinePay($out_trade_no, $pay_type){
        
        $v_order_model = new NvOrderModel();
        $v_order_model->startTrans();
        
        try {
        
            $order_info = $this->getOrderInfo(0, ['out_trade_no' => $out_trade_no]);
           
            //修改订单状态
            $v_order_model->save(['order_status' => 1,'payment_type' => $pay_type,'pay_time'=>time(),'status_time'=>time()], ['out_trade_no' => $out_trade_no]);
        
            //修改订单项状态
            $v_order_item_model = new NvOrderItemModel();
            $v_order_item_model->save(['order_status' => 1], ['v_order_id' => $order_info['v_order_id']]);
        
            //支付成功发放拥有课程(或只是课程章节)及其套餐
            $v_member_service = new Vmember();
            $v_member_service->addVmemberOrderByHave($order_info['v_order_id']);
            
            //初步只要支付就为完成状态
            $this->orderComplete($order_info['v_order_id']);
            
            $v_order_model->commit();
            return 1;
        } catch (\Exception $e) {
        
            $v_order_model->rollback();
            Log::write('订单线上支付成功后执行报错，错误：'. $e->getMessage());
            return $e->getMessage();
        }
    }
    
    /**
     * 创建订单流水号
     */
    public function createOrderNo(){
        
        $time_str = date('Ymd');
        $v_order_model = new NvOrderModel();
        $order_obj = $v_order_model->getFirstData('1=1', "v_order_id DESC");
        $num = 0;
        if (! empty($order_obj)) {
            $order_no_max = $order_obj["order_no"];
            if (empty($order_no_max)) {
                $num = 1;
            } else {
                if (substr($time_str, 0, 8) == substr($order_no_max, 0, 8)) {
                    $max_no = substr($order_no_max, 8, 7);
                    $num = $max_no * 1 + 1;
                } else {
                    $num = 1;
                }
            }
        } else {
            $num = 1;
        }
        $order_no = $time_str . sprintf("%07d", $num);
        return $order_no;
    }

    /**
     * 创建订单支付编号
     *
     * @param unknown $order_id
     */
    public function createOutTradeNo()
    {
        $pay_no = new UnifyPay();
        return $pay_no->createOutTradeNo();
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOrder::orderComplete()
     */
    public function orderComplete($order_id){
       
        $order_model = new NvOrderModel();
        $order_model->startTrans();
        
        try {
            
            //修改订单状态
            $res = $order_model->save(['order_status' => 2,'status_time'=>time()], ['v_order_id' => $order_id]);
            
            //获取订单下的订单项
            $order_item_list = $this->getOrderItemList($order_id);
            foreach ($order_item_list as $item){
                
                //修改订单项状态
                $this->orderItemComplete($item['order_item_id']);
                
                //根据订单项计算销量
                $this->setOrderItemBuyCount($item['order_item_id']);
                
            }
            
            $order_model->commit();
            return $res;
            
        } catch (\Exception $e) {
            
            $order_model->rollback();
            return $e->getMessage();
        }
      
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOrder::orderItemComplete()
     */
    public function orderItemComplete($order_item){
        
        $order_item_model = new NvOrderItemModel();
        $res = $order_item_model->save(['status_time' => time(), 'order_status'=>2], ['order_item_id' => $order_item]);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOrder::setOrderItemBuyCount()
     */
    public function setOrderItemBuyCount($order_item){
        
        $order_item_info = $this->getOrderItemInfo($order_item);
        $relation_id = $order_item_info['relation_id'];
        $item_type = $order_item_info['item_type'];
        
        //获取同类型的购买数量
        $complete_count = $this->getOrderItemCount($item_type, $relation_id);
        
        if($item_type == 1){//课程
            
            $courser_model = new NvCourseModel();
            $courser_model->save(['buy_count'=> $complete_count], ['course_id' => $relation_id]);
            
        }elseif ($item_type == 2){//套餐
            
            $package_model = new NvPackageModel();
            $package_model->save(['buy_count'=> $complete_count], ['package_id' => $relation_id]);
        }
        
        return 1;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOrder::getOrderItemCount()
     */
    public function getOrderItemCount($item_type, $relation_id){
        
        $order_item_model = new NvOrderItemModel();
        $complete_count = $order_item_model->getCount(['item_type'=> $item_type, 'relation_id' => $relation_id]);
        
        return $complete_count;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOrder::getOrderRelationListMoney()
     */
    public function getOrderRelationListMoney($order_relation_list){
        
        $order_relation_arr = json_decode($order_relation_list, true);
        
        $order_money = 0;
        $course_service = new Course();
        
        foreach($order_relation_arr as $item){
            
            $relation_type = $item['relation_type'];
            if($relation_type == 1){ //课程价格
                
                if(!empty($item['kpoint_id'])){
                    
                    $order_money = $course_service->getCourseKpointInfo($item['kpoint_id'])['kpoint_price'];
                }else{
                    $course_id = $item['relation_id'];
                    $order_money = $course_service->getCourseInfo($course_id, '', 'current_price')['current_price'];
                }
                
            }elseif($relation_type == 2){ //套餐价格
            
                $package_id = $item['relation_id'];
                $order_money = $course_service->getPackageInfo($package_id, '', 'current_price')['current_price'];
            }
        }
       
        return $order_money;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOrder::getOrderRelationItemList()
     */
    public function getOrderRelationItemList($order_relation_list){
        
        $order_relation_arr = json_decode($order_relation_list, true);

        $order_money = 0;
        $order_item_list = array();
        $course_service = new Course();
        
        foreach($order_relation_arr as $item){
            
            $relation_type = $item['relation_type'];
            if($relation_type == 1){ //课程价格
            
                $course_id = $item['relation_id'];
                $course_info = $course_service->getCourseInfo($course_id, '', 'cover_img,course_title,course_dec,current_price');
                
                $order_item = array(
                    'cover_img' => $course_info['cover_img'],
                    'title' => $course_info['course_title'],
                    'description' => $course_info['course_dec'],
                    'price' => $course_info['current_price']
                );
                
                $kpoint_id = $item['kpoint_id'];
                if(!empty($kpoint_id)){
                    $kpoint_info = $course_service->getCourseKpointInfo($kpoint_id, '', 'kpoint_name,kpoint_price');
                    $order_item['kpoint_name'] = $kpoint_info['kpoint_name'];
                    $order_item['price'] = $kpoint_info['kpoint_price'];
                }
            
                $order_item_list[] = $order_item;
            
            }elseif($relation_type == 2){ //套餐价格
            
                $package_id = $item['relation_id'];
                $package_info = $course_service->getPackageInfo($package_id, '', 'package_title,cover_img,package_dec,current_price');
                
                $order_item = array(
                    'cover_img' => $package_info['cover_img'],
                    'title' => $package_info['package_title'],
                    'description' => $package_info['package_dec'],
                    'price' => $package_info['current_price']
                );
            
                $order_item_list[] = $order_item;
            }
        }
        
        return $order_item_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOrder::setOrderCouponReplaceMoney()
     */
    public function setOrderCouponReplaceMoney($mc_id, $order_money){
        
        if(empty($mc_id)) return $order_money;
        
        $vmember_account = new VmemberAccount();
        $member_coupon_info = $vmember_account->getMemberCouponInfo($mc_id);
        
        if(empty($member_coupon_info)) return NV_COUPON_NO_EXIST;  //会员无该优惠劵
        if($member_coupon_info['status'] == 1) return NV_COUPON_USE; //已被使用
        if($member_coupon_info['status'] == -1) return NV_COUPON_EXPIRED; //已过期
        
        $operate_service = new Operate();
        $coupon_info = $operate_service->getCouponInfo($member_coupon_info['coupon_id']); //获取优惠劵信息
        
        $discount_type = $coupon_info['discount_type'];
        $condition_money = $coupon_info['condition_money']; //达到的条件金额
        
        $coupon_replace_money = 0.00;

        //是否达到消费金额
        if($order_money < $condition_money && $condition_money != 0.00){
        
            return NV_COUPON_IS_CONDITION;
        }
        
        if($discount_type == 1){
        
            $order_money = $order_money - $coupon_info['type_value'];
            //如果顶替的金额大于订单金额就以订单为主
            if($coupon_replace_money > $order_money) $order_money = 0.00;
            
        
        }else if($discount_type == 2){
        
            $order_money = sprintf("%.2f",($order_money * $coupon_info['type_value'] / 100));
        }
        
        return $order_money;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IOrder::setOrderStatusCancel()
     */
    public function setOrderStatusCancel($v_order_id){
        
        $condition = array(
            'v_order_id'     => $v_order_id,
            'uid'            => $this->uid,
            'order_status'   => 0
        );
        
        $order_model = new NvOrderModel();
        $order_model->startTrans();
        
        try {
            
            //修改订单状态
            $v_order_info = $this->getOrderInfo($v_order_id);
            $res = $order_model->save(['order_status'=>-1], $condition);
            
            //修改订单项状态
            $order_item_model = new NvOrderItemModel();
            $order_item_model->save(['order_status'=>-1], $condition);
            
            $balance = $v_order_info['balance'];        //使用余额
            $mc_id = $v_order_info['mc_id'];            //优惠劵id
            
            //取消订单退还余额
            if($balance > 0){
                
                $account_flow = new MemberAccount();
                $account_flow->addMemberAccountData(0, 2, $this->uid, 1, $balance, 22, $v_order_id, '取消课程订单');
            }
            
            //如果使用的优惠劵的话退还优惠劵
            if(!empty($mc_id)){
                
                $v_member_account_service = new VmemberAccount();
                $v_member_account_service->setMemberCouponReturn($mc_id);
            }
            
            
            $order_model->commit();
            return $res;
        } catch (\Exception $e) {
            
            $order_model->rollback();
            Log::write('取消订单失败，错误为'.$e->getMessage());
            return $e->getMessage();
        }
        
    }
    
    
    public function getOrderCount($condition)
    {
        $order = new NvOrderModel();
        $count = $order->where($condition)->count();
        return $count;
    }
    
    
}