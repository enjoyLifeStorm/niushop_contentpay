<?php
/**
 * IVmemberAccount.php
 *
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2017年9月18日
 * @version : v1.0.0.0
 */
namespace data\worksservice;
use data\service\BaseService;
use data\worksmodel\NvMemberEvaluateModel;
use data\worksmodel\NvMemberDianzanRecordModel;
use data\worksmodel\NvMemberCollectionModel;
use data\worksmodel\NvCourseModel;
use data\worksmodel\NvMemberStudyNoteModel;
use data\worksmodel\NvMemberStudyHistoryModel;
use data\worksmodel\NvMemberCourseDiscussModel;
use data\service\Member;
use think\Log;
use data\worksmodel\NvMemberCouponModel;
use data\worksmodel\NvMemberHaveModel;

class VmemberAccount extends BaseService implements \data\worksapi\IVmemberAccount
{
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IVmemberAccount::addUpdateVmemberStudyHistory()
     */
    public function addUpdateVmemberStudyHistory($kpoint_id, $play_stop_time){
        
        $study_history_model = new NvMemberStudyHistoryModel();
        $study_history_info = $this->getVmemberStudyHistoryInfo($kpoint_id, 'id,course_id,time_length,max_play_time');
        $play_stop_time = sprintf("%.2f", $play_stop_time);
        
        $data = array(
            'uid'   => $this->uid,
            'kpoint_id' => $kpoint_id,
            'end_play_time' => time(),
            'play_stop_time'    => $play_stop_time
        );
        
        if(empty($study_history_info)){
            
            //获取当前学习的章节信息
            $course_service = new Course();
            $kpoint_info = $course_service->getCourseKpointInfo($kpoint_id,'','course_id,time_length,parent_id');
            
            //添加课程id
            $data['course_id'] = $kpoint_info['course_id'];
            $data['time_length'] = $kpoint_info['time_length'];
            $data['max_play_time'] = $play_stop_time; //最大播放进度
            $data['parent_kpoint_id'] = $kpoint_info['parent_id'];
            
            $res = $study_history_model->save($data);
            
        }else{
            
            //如果最后的视频学习播放时间和视频时长相等，会员已经学完
            if($play_stop_time == $study_history_info['time_length']){
                $data['is_study_finish'] = 1;
                $data['max_play_time'] = $study_history_info['time_length']; //播放完以后最大播放进度为视频时长
            }
            
            //如果原有最大播放进度小于现在的播放停止时间就修改
            if($study_history_info['max_play_time'] < $play_stop_time){
                $data['max_play_time'] = $play_stop_time; //最大播放进度
            }
            
            $res = $study_history_model->save($data, ['id' => $study_history_info['id']]);
            
        }
        
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\api\video\IVmemberAccount::getVmemberStudyHistoryInfo()
     */
    public function getVmemberStudyHistoryInfo($kpoint_id, $field = '*'){
        
        $condition = array(
            'kpoint_id' => $kpoint_id,
            'uid'       => $this->uid
        );
        
        $study_history_model = new NvMemberStudyHistoryModel();
        $study_history_info = $study_history_model->getInfo($condition, $field);
        return $study_history_info;
    }
    
    public function getVmemberStudyHistoryList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
        
        $study_history_model = new NvMemberStudyHistoryModel();
        $condition['uid'] = $this->uid;
        $study_history_list = $study_history_model->getViewList($page_index, $page_size, $condition, $order, $field);
        
        return $study_history_list;
        
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::addMemberEvaluate()
     */
    public function addMemberEvaluate($relation_type, $relation_id, $evaluate_text, $reply_evaluate = 0){
       
        //获取评价会员信息
        $member_service = new Member();
        $user_info = $member_service->getUserInfo();
        
        $member_evaluate_model = new NvMemberEvaluateModel();
        $member_evaluate_model->startTrans();
        
        try {
            
            $data = array(
                'uid'   => $this->uid,
                'relation_type' => $relation_type,
                'relation_id'   => $relation_id,
                'evaluate_text' => $evaluate_text,
                'create_time'   => time(),
                'user_name'     => $user_info['nick_name']?$user_info['nick_name']:$user_info['user_name'],
                'user_head_img' => $user_info['user_headimg'],
                'reply_evaluate'=> $reply_evaluate
            );
            
            $res = $member_evaluate_model->save($data);
            
            //如果是回复就计算评价的回复数量
            if(!empty($reply_evaluate)) $this->setMemberEvaluateReplyCount($reply_evaluate);
            
            $member_evaluate_model->commit();
            return $res;
            
        } catch (\Exception $e) {
            
            $member_evaluate_model->rollback();
            return $e->getMessage();
        }
        
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::setMemberEvaluateReplyCount()
     */
    public function setMemberEvaluateReplyCount($evaluate_id){
        
        //获取被回复数量
        $member_evaluate_model = new NvMemberEvaluateModel();
        $reply_count = $member_evaluate_model->getCount(['reply_evaluate' => $evaluate_id]);
        
        $res = $member_evaluate_model->save(['reply_count'=>$reply_count], ['evaluate_id' => $evaluate_id]);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::getMemberEvaluateList()
     */
    public function getMemberEvaluateList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
        
        $member_evaluate_model = new NvMemberEvaluateModel();
        $member_evaluate_list = $member_evaluate_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        return $member_evaluate_list;
    }
    
    /**
     * (non-PHPdoc)如果哪项设置点赞赠送积分需要在这查询添加
     * @see \data\worksapi\IVmemberAccount::addMemberDianzanRecord()
     */
    public function addMemberDianzanRecord($type, $relation_id){
        
        $member_dianzan_model = new NvMemberDianzanRecordModel();
        
        $condition = array(
            'type'          => $type,
            'uid'           => $this->uid,
            'relation_id'   => $relation_id
        );
        $praise_info = $member_dianzan_model->getInfo($condition,'zan_id');
        if($praise_info){
            return 0;
        }
       
        $give_point = 0;
        $data = array(
            'type'          => $type,
            'uid'           => $this->uid,
            'relation_id'   => $relation_id,
            'give_point'    => $give_point,
            'create_time'   => time()
        );
        
        $res = $member_dianzan_model->save($data);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::setMemberDianzanCount()
     */
    public function setMemberDianzanCount($type, $relation_id){
        
        $member_dianzan_model = new NvMemberDianzanRecordModel();
        
        $dianzan_count = $member_dianzan_model->getCount(['type' => $type, 'relation_id'=> $relation_id]);
        
        if($type == 1){
            
            $member_evaluate_model = new NvMemberEvaluateModel();
            $member_evaluate_model->save(['spot_praise_num'=> $dianzan_count], ['evaluate_id' => $relation_id]);
        }
        
        return $dianzan_count;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::addMembercollection()
     */
    public function addMemberCollection($type, $relation_id, $label_id = 0){
        
        
        $member_collection_model = new NvMemberCollectionModel();
        $member_collection_model->startTrans();
        
        try {
            
            $collection_info = $member_collection_model->getInfo(['type'=>$type, 'relation_id'=>$relation_id, 'uid'=>$this->uid],'*');
            
            if(!empty($collection_info)){
                $member_collection_model->commit(); 
                return 1;
            }
            
            $course_service = new Course();
            $title = '';
            $cover_img = '';
            $price  = 0.00;
            if($type == 1){  //如果为课程
            
                $course_info = $course_service->getCourseInfo($relation_id,'','course_title,cover_img,current_price,course_dec');
                $title = $course_info['course_title'];
                $cover_img = $course_info['cover_img'];
                $price = $course_info['current_price'];
                $dec = $course_info['course_dec'];
            }elseif($type == 2){  //套餐
            
                $package_info = $course_service->getPackageInfo($relation_id,'','package_title,cover_img,current_price,package_dec');
                $title = $package_info['package_title'];
                $cover_img = $package_info['cover_img'];
                $price = $package_info['current_price'];
                $dec = $package_info['package_dec'];
            }
            
            $data = array(
                'type'          => $type,
                'relation_id'   => $relation_id,
                'uid'           => $this->uid,
                'title'         => $title,
                'cover_img'     => $cover_img,
                'price'         => $price,
                'label_id'      => $label_id,
                'create_time'   => time(),
                'dec'           =>$dec
            );
            
            $member_collection_model->save($data);
            
            //计算收藏数量
            $this->setCollectionCount($type, $relation_id);
            
            $member_collection_model->commit();
            return 1;
        } catch (\Exception $e) {
            
            Log::write('会员添加收藏失败，错误：' . $e->getMessage());
            $member_collection_model->rollback();
            return $e->getMessage();
        }
      
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::getMemberCollectionList()
     */
    public function getMemberCollectionList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
     
        $condition['uid'] = $this->uid;   //会员只能查看自己的收藏记录
        
        $member_collection_model = new NvMemberCollectionModel();
        $member_collection_list = $member_collection_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        
        return $member_collection_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::delMemberCollection()
     */
    public function delMemberCollection($type, $relation_id){
        
        $condition = array(
            'type'      => $type,
            'relation_id'   => $relation_id,
            'uid'       => $this->uid
        );
        $member_collection_model = new NvMemberCollectionModel();
        $member_collection_model->startTrans();
        
        try {
            
            $member_collection_model->destroy($condition);

            //计算收藏数量
            $this->setCollectionCount($type, $relation_id);
            
            $member_collection_model->commit();
            return 1;
        } catch (\Exception $e) {
            
            Log::write('删除会员收藏失败，错位：'. $e->getMessage());
            $member_collection_model->rollback();
            return $e->getMessage();
        }
        
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::setCollectionCount()
     */
    public function setCollectionCount($type, $relation_id){
        
        $condition = array(
            'type'      => $type,
            'relation_id'   => $relation_id
        );
        
        $member_collection_model = new NvMemberCollectionModel();
        $collection_count = $member_collection_model->getCount($condition);
        
        if($type == 1){  //课程
            
            $course_model = new NvCourseModel();
            $course_model->save(['collection_count' => $collection_count], ['course_id' => $relation_id]);
        }elseif($type == 2){  //套餐
            
            $course_service = new Course();
            $course_service->addUpdatePackage(['package_id' => $relation_id, 'collection_count' => $collection_count]);
        }
        
        return 1;
    }
    
    
    public function isCourseCollection($relation_id , $type = 1 , $field = '*'){
        $condition = array(
            'relation_id' => $relation_id,
            'uid'       => $this->uid
        );
        
        $member_collection_model = new NvMemberCollectionModel();
        $collection_info = $member_collection_model->getInfo($condition, $field);
        
        return $collection_info;
    }
    
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::addUpdateMemberStudyNote()
     */
    public function addUpdateMemberStudyNote($note_id, $course_id, $course_title, $kpoint_id, $note_text, $works_play_time){
        
        //根据目录获取学习的目录信息
        
        $v_member_study_note_model = new NvMemberStudyNoteModel();
        $data = array(
            'note_text'     => $note_text,
            'course_id'     => $course_id,
            'course_title'  => $course_title,
            'kpoint_id'     => $kpoint_id,
            'works_play_time'=> $works_play_time,
            'create_time'   => time()
        );
        
        if(empty($note_id)){
            $data['uid'] = $this->uid;
            $note_id = $v_member_study_note_model->save($data);
            
            return $note_id;
        }else{
        
            $res = $v_member_study_note_model->save($data, ['note_id'=>$note_id,'uid'=>$this->uid]);
            
            return $res;
        }
        
        
    }

    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::getMemberStudyNoteInfo()
     */
    public function getMemberStudyNoteInfo($note_id){
        
        $v_member_study_note_model = new NvMemberStudyNoteModel();
        $study_note_info = $v_member_study_note_model->getInfo(['note_id' => $note_id], '*');
        return $study_note_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::getMemberStudyWorksNoteInfo()
     */
    public function getMemberStudyWorksNoteInfo($works_code){
        
        $v_member_study_note_model = new NvMemberStudyNoteModel();
        $study_note_list = $v_member_study_note_model->getQuery(['works_code' => $works_code], '*', 'create_time desc');
        return $study_note_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::getMemberStudyNoteList()
     */
    public function getMemberStudyNoteList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
        
        $course_service = new Course();
        
        $v_member_study_note_model = new NvMemberStudyNoteModel();
        $study_note_list = $v_member_study_note_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        
        $v_member_have_model = new NvMemberHaveModel();
        
        
        foreach($study_note_list['data'] as $item){
            
            $course_kpoint_info = $course_service->getCourseKpointInfo($item['kpoint_id'],'','kpoint_name,course_id,parent_id');
            $parent_kpoint_info = $course_service->getCourseKpointInfo($course_kpoint_info['parent_id'],'','kpoint_name,course_id');
            //$course_info = $course_service->getCourseInfo($course_kpoint_info['course_id'],'','course_title');
            
            $condition['uid'] = $this->uid;
            $condition['relation_id'] = $item['course_id'];
            $v_member_have_info = $v_member_have_model->getInfo($condition, 'have_id');
            
            $item['kpoint_name'] = $course_kpoint_info['kpoint_name'];
            $item['parent_kpoint_name'] = $parent_kpoint_info['kpoint_name'];
            //$item['course_title'] = $course_info['course_title'];
            $item['have_id'] = $v_member_have_info['have_id'];
        }
        return $study_note_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::delMemberStudyNote()
     */
    public function delMemberStudyNote($note_id){
        
        $condition = array(
            'note_id' => $note_id,
            //'uid'     => $this->uid
        );
        
        $vmember_study_note_model = new NvMemberStudyNoteModel();
        $res = $vmember_study_note_model->destroy($condition);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::getMemberKpointStudyNoteList()
     */
    public function getMemberKpointStudyNoteList($kpoint_id){
        
        $vmember_study_note_model = new NvMemberStudyNoteModel();
        
        $condition = array(
            'uid'   => $this->uid,
            'kpoint_id' => $kpoint_id
        );
        $study_note_list = $vmember_study_note_model->getQuery($condition, '*', 'create_time desc');
        return $study_note_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::addMemberCourseDiscuss()
     */
    public function addMemberCourseDiscuss($course_id, $kpoint_id, $discuss_text){
        
        $course_discuss_model = new NvMemberCourseDiscussModel();
        $data = array(
            'uid'       => $this->uid,
            'course_id' => $course_id,
            'kpoint_id' => $kpoint_id,
            'discuss_text'  => $discuss_text,
            'create_time'   => time()
        );
        
        $res = $course_discuss_model->save($data);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::getMemberCourseDiscussList()
     */
    public function getMemberCourseDiscussList($course_id){
        
        $course_discuss_model = new NvMemberCourseDiscussModel();
        $course_discuss_list = $course_discuss_model->getQuery(['course_id'=>$course_id], '*', 'create_time desc');
        
        foreach($course_discuss_list as $item){
            
            $member_service = new Member();
            $item['user_name'] = $member_service->getUserInfoByUid($item['uid'])['user_name'];
            $item['head_img'] = $member_service->getMemberImage($item['uid']);
        }
        return $course_discuss_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::getMemberCourseDefaultPlayKpoint()
     */
    public function getMemberCourseDefaultPlayKpoint($course_id){
       
     
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::addMemberCoupon()
     */
    public function addMemberCoupon($vcoupon_id){
       
        $vmember_coupon_model = new NvMemberCouponModel();
        $vmember_coupon_model->startTrans();
        
        try {
            
            $coupon_service = new Operate();
            $coupon_info = $coupon_service->getCouponInfo($vcoupon_id,['status'=>1]);
            if(empty($coupon_info)) return NV_COUPON_NO_EXIST;  //可领取的优惠劵是否存在
            
             
            
            //优惠劵存在领取上限的话判断
            if(!empty($coupon_info['each_max_num'])){
            
                $member_coupon_count = $this->getMemberCouponCount(['coupon_id'=>$vcoupon_id]);
                if($coupon_info['each_max_num'] <= $member_coupon_count && $coupon_info['each_max_num'] != 0){
            
                    return NV_MEMBER_COUPON_ALREADY_MAX;
                }
            }
            
            $data = array(
                'uid' => $this->uid,
                'coupon_id' => $vcoupon_id,
                'status' => 0,
                'create_time' => time()
            );
            
            //领取的优惠劵计算结束时间
            if($coupon_info['receive_end_type'] == 1){
            
                $data['end_time'] = time() + ($coupon_info['receive_end_time'] * 24 * 60 * 60);
            
            }elseif($coupon_info['receive_end_type'] == 2){
            
                $data['end_time'] = $coupon_info['receive_end_time'];
            }
            if(empty($coupon_info['receive_end_time'])) $data['end_time'] = 0; // 如果无结束时间限制
            
            $res = $vmember_coupon_model->save($data);
            
            $coupon_service->setCouponSurplusNum($vcoupon_id);
            
            $vmember_coupon_model->commit();
            return $res;
        } catch (\Exception $e) {
            
            $vmember_coupon_model->rollback();
            Log::write('会员领取优惠劵出错， 错误为：'. $e->getMessage());
            return $e->getMessage();
        }
       
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::getMemberCouponInfo()
     */
    public function getMemberCouponInfo($mc_id){
        
        $condition  = array(
            'uid' => $this->uid,
            'mc_id' => $mc_id
        );
        
        $vmember_coupon_model = new NvMemberCouponModel();
        $vmember_coupon_info = $vmember_coupon_model->getInfo($condition, '*');
        
        if(!empty($vmember_coupon_info)){
            
            $operate_service = new Operate();
            $coupon_info = $operate_service->getCouponInfo($vmember_coupon_info['coupon_id']);
            $vmember_coupon_info['title'] = $coupon_info['title'];     //优惠劵标题
            $vmember_coupon_info['discount_type'] = $coupon_info['discount_type'];     //优惠类型
            $vmember_coupon_info['type_value'] = $coupon_info['type_value'];     //优惠类型对应值
            $vmember_coupon_info['condition_money'] = $coupon_info['condition_money'];     //条件
        }
        return $vmember_coupon_info;
    }
    
    public function getMemberCouponByCouponid($coupon_id){
        $condition = array(
            'coupon_id'=>$coupon_id,
            'uid'=>$this->uid
        );
        $vmember_coupon_model = new NvMemberCouponModel();
        $vmember_coupon_info = $vmember_coupon_model->getInfo($condition, '*');
        return $vmember_coupon_info;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::getMemberCouponList()
     */
    public function getMemberCouponList($page_index = 1, $page_size = 0, $condition = '', $order = '', $field = '*'){
     
        $vmember_coupon_model = new NvMemberCouponModel();
        $condition['uid'] = $this->uid;
        $vmember_coupon_list = $vmember_coupon_model->pageQuery($page_index, $page_size, $condition, $order, $field);
        
        $operate_service = new Operate();
        foreach($vmember_coupon_list['data'] as $item){
            
            $coupon_info = $operate_service->getCouponInfo($item['coupon_id']);
            
            $item['title'] = $coupon_info['title'];     //优惠劵标题
            $item['discount_type'] = $coupon_info['discount_type'];     //优惠类型
            $item['type_value'] = $coupon_info['type_value'];     //优惠类型对应值
            $item['condition_money'] = $coupon_info['condition_money'];     //条件
            
            //状态名称
            $status_name = '';
            switch ($item['status']) {
                case 1:
                    $status_name = '已使用';
                    break;
                case -1:
                    $status_name = '已过期';
                    break;
                default:
                    $status_name = '未使用';
                    break;
            }
            $item['status_name'] = $status_name;
        }
        return $vmember_coupon_list;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::delMemberCoupon()
     */
    public function delMemberCoupon($mc_id){
     
        $condition = array(
            'uid' => $this->uid,
            'mc_id' => $mc_id
        );
        
        $vmember_coupon_model = new NvMemberCouponModel();
        $res = $vmember_coupon_model->destroy($condition);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::getMemberCouponCount()
     */
    public function getMemberCouponCount($condition = ''){
     
        $condition['uid'] = $this->uid;
        
        $vmember_coupon_model = new NvMemberCouponModel();
        $have_coupon_count = $vmember_coupon_model->getCount($condition);
        return $have_coupon_count;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::setMemberCouponUse()
     */
    public function setMemberCouponUse($mc_id){
        
        $condition = array(
            'mc_id'     => $mc_id,
            'status'    => 0,
            'uid'       => $this->uid
        );
        $vmember_coupon_model = new NvMemberCouponModel();
        $res = $vmember_coupon_model->save(['status'=>1], $condition);
        return $res;
    }
    
    /**
     * (non-PHPdoc)
     * @see \data\worksapi\IVmemberAccount::setMemberCouponReturn()
     */
    public function setMemberCouponReturn($mc_id){
       
        $condition = array(
            'mc_id'     => $mc_id,
            'status'    => 1,
            'uid'       => $this->uid
        );
        $vmember_coupon_model = new NvMemberCouponModel();
        $res = $vmember_coupon_model->save(['status'=>0], $condition);
        return $res;
    }
}