$(function() {
	vP(); // 放器高度动态赋值
	//sB(studyPercent); // 学习进度动画
	//shareShow(); // 课程分享
	//treeMenu3(); //课程树
	//replyFun(); // 回复展开
	//ocFun(); // 开关灯效果
	cardChange("#p-h-r-title>li", "#p-h-r-cont>section", "current"); // 菜单选项卡
	browserRedirect(); // 右侧不同设备下显隐
	//queryComment();// 课程评论
	// 加载 笔记编辑器
//	initKindEditornote();
	// 学过此课程的用户
	//getCourseLearnedUser(courseId);
});


$(window).resize(function() {
	if(checkIsMobile()) {// 移动端环境下效果
		/*var wH = parseInt(document.documentElement.clientHeight, 10);
		 //$("#p-h-box").css("height", wH - 258);
		 $("#p-h-r-cont").css("height", wH - 363);
		 $(".p-h-video").css("height", wH - 330);*/
	}else{
		vP();
	}
});

// 播放器高度动态赋值
var vP = function() {
	var wH = parseInt(document.documentElement.clientHeight, 10);
    if(checkIsMobile()){
        $(".n-p-v-b-rt-in").css("height","auto");
	}else{
        $(".n-p-v-b-rt-in").css("height", wH - 60);
	}
	$(".p-h-r-list .lh-menu-new").css("height", wH - 130);
    $(".p-h-r-list .lh-menu-new-tl").css("height", wH - 300);
    if(checkIsMobile()){
        $("#my-video").css("height", "240px");
    }else {
        $("#my-video").css("height", wH - 60);
    }
	//讨论动态高度赋值
	$("#p-h-r-cont .p-center-discuss,#p-h-r-cont .talkhtml").height($("#p-h-r-cont").height()-110);
};

// 右侧菜单区域显示与隐藏
var rmFun = function() {
    $(".video-open-btn>a").bind("click",function() {
		var _this = $(this);
		var _index;
		if (!_this.hasClass("current")) {
			
			$(".n-p-v-b-rt").animate({"right" : "0px"}, 400);
			$(".n-p-vb-in").animate({"padding-right" : "360px"}, 400);
			_this.parent(".video-open-btn").animate({right:"360px"},400);
            _this.addClass("current").siblings().removeClass("current");
                _index = $(".video-open-btn>a").index(this);
                var __this = $(".n-p-v-b-rt-in>div").eq(_index);
                __this.find(".lh-menu-new").niceScroll({
                    cursorcolor:"#e7542a",
                    cursoropacitymax:1,
                    touchbehavior:false,
                    cursorwidth:"3px",
                    cursorborder:"0",
                    cursorborderradius:"3px"
                });
                $(".n-p-v-b-rt-in>div").eq(_index).show().siblings().hide();
            	$(".n-p-v-b-rt-in>div").eq(_index).find(".lh-menu-new").getNiceScroll().show();
            	$(".n-p-v-b-rt-in>div").eq(_index).siblings().find(".lh-menu-new").getNiceScroll().hide();
            plejName();
		} else {
			$(".n-p-v-b-rt").animate({"right" : "-360px"}, 500);
			$(".n-p-vb-in").animate({"padding-right" : "0px"}, 400);
			_this.parent(".video-open-btn").animate({right:"0px"},400);
            _this.removeClass("current");
            $(".n-p-v-b-rt-in>div").find(".lh-menu-new").getNiceScroll().remove();
		}
	})
};

// 开关灯效果
var ocFun = function() {
	var bMask = $('<div class="bMask"></div>');
	bMask.css({
		"opacity" : "0.8"
	});
	$(".dpBtn").click(function() {
		var _this = $(this);
		if (!_this.hasClass("dpOpen")) {
			$("body").prepend(bMask);
			_this.addClass("dpOpen");
			_this.children("a").text("开灯").attr("title", "开灯");
		} else {
			bMask.remove();
			_this.removeClass("dpOpen");
			_this.children("a").text("关灯").attr("title", "关灯");
		}
	})
};
// 移动端显示
function browserRedirect() {
	if(checkIsMobile()){   // 移动端环境下效果
		$(".mo-v-tab-tit-list li").bind("click",function () {
            var _this = $(this);
            var _index;
            if(!_this.hasClass("current")){
                _this.addClass("current").siblings().removeClass("current");
                _index = $(".mo-v-tab-tit-list li").index(this);
                $(".n-p-v-b-rt-in>div").eq(_index).show().siblings().hide();
			}else {

			}
        });
        $(".n-p-v-b-rt-in>div").find(".lh-menu-new").getNiceScroll().hide();
        $(".p-h-r-list .lh-menu-new").css({"height": "auto","width": "100%;"});
        plejName();
	}else { // 非移动端环境下效果
		rmFun();
	}
}
function plejName() {
	return;
	var ejWidth = $(".pl-v-r-m-rj-warp").width();
	console.log(ejWidth);
	$(".pl-v-ej-name").css("width",ejWidth);
}
//播放大厅右侧删除弹框
function videoMsk(courseNoteId) {
	var maskBox = $(".n-p-vid-mask");
    $(".n-p-vid-mask").animate({"top" : "0px"}, 400);
    $(".n-p-vid-coles").bind("click",function () {
        maskBox.animate({"top" : "-100%"}, 400);
    })
	$("#deleteCourseNoteTag").attr("onclick","deleteCourseNote("+courseNoteId+")");
}
/**
 * 收藏课程
 * @param courseId
 *            课程ID
 */
function favorites(courseId,obj) {
	$.ajax({
		url : baselocation + '/front/createfavorites/' + courseId,
		type : 'post',
		dataType : 'json',
		success : function(result) {
			if (result.success == false) {
				dialog('提示', result.message, 1);
			} else {
				$(obj).html("已收藏").attr("title","已收藏").parent().addClass("sc-end");
				dialog('提示', result.message, 0);
			}
		}
	});
}
/**
  * 笔记编辑器初始化
  */
 function initKindEditornote(id){//编辑器初始化
 	KindEditor.create('textarea[id="'+id+'"]', {
 		filterMode : true,// true时过滤HTML代码，false时允许输入任何代码。
 		pasteType:1,//设置粘贴类型，0:禁止粘贴, 1:纯文本粘贴, 2:HTML粘贴
 		allowPreviewEmoticons : false,
 		syncType : 'auto',
 		width : '99%',
 		minWidth : '10px',
 		minHeight : '10px',
 		height : '300px',
 		urlType : 'domain',// absolute
 		newlineTag : 'br',// 回车换行br|p
 		uploadJson : keuploadSimpleUrl+'&param=note',//图片上传路径
 		allowFileManager : false,
 		afterBlur : function() {
 			this.sync();
 		},
 		items : [  'forecolor', 'emoticons'],
 		afterChange : function() {
 			$('#numweiBoContent').html(140 - this.count('text'));
 			var num = 140 - this.count('text');
 			
 			if (num >= 0) {
 				$(".error").html("您还可以输入"+num+"字");
 			} else {
 				$(".error").html("您输入的文字超过140字！");
 				$('#wenzistrweiBoContent').html("你已经超过");
 				num = -num;
 			}
 			$('#numweiBoContent').html(num);
     		}
     	});
 }

function clickNote(obj){
	$(".note_html").html("");
	$(".course_mulu").click();
	$(obj).removeClass("current").siblings().removeClass("current");
	$(obj).addClass("current");
	$(".commentHtml").next(".courseKpointHtml").hide();
	$(".commentHtml").show();
	$.ajax({
		url : baselocation + "/courseNote/ajax/querynote",
		type : "post",
		dataType : "text",
		data : {
			'kpointId' : currentKpointId,
			"courseId" : courseId
		},
		success : function(result) {
			$(".commentHtml").html(result);
		}
	});

}




