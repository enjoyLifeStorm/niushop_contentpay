<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\workswap\controller;
use data\service\Article;
use data\service\WebSite as Website;
use data\worksservice\Website as WebsiteService;
use data\worksservice\Course as CourseService;
use data\worksservice\yiwentang;

class Index extends BaseController
{
    /**
     * 手机端首页
     *
     * @return Ambigous <\think\response\View, \think\response\$this, \think\response\View>
     */
    public function index()
    {
        $web_site = new Website;
        $website = new WebsiteService();
        
        // 获取网站相关信息
        $web_info = $web_site->getWebSiteInfo();
        $this->assign('web_info', $web_info);
        
        // 首页轮播图 同上 严格来讲应该根据广告位查询广告
        $adv_list = $website->getWebsiteAdvList(1,0,['ap_id'=>12],'slide_sort asc');
        $this->assign('adv_list',$adv_list['data']);
        
        // 首页顶部导航 严格来讲应该根据导航位查询导航
        $navigation_list = $website->getWebsiteNavigationList(1,0,['position_id'=>16],'sort asc');
        $this->assign('navigation_list',$navigation_list['data']);
        
        // 首页版块
        $condition_index['plate_type'] =2;
        $index_block = $website->getHomePlateList(['plate_type'=>2]);
        $this->assign('index_block',$index_block);
        
        //专业楼层显示
        $subject_home_block = $website->getSubjectHomeBlock();
        $this->assign("subject_home_block", $subject_home_block);
        
        return view($this->style . 'Index/index');
        
    }
    
    /**
     * 客服
     */
    public function customer(){
        
        $yiwentang = new yiwentang();
        $kefu_info = $yiwentang->getYwtKefu();
        $kefu_info = json_decode($kefu_info,true);
        $this->assign('kefu_info',$kefu_info);
        return view($this->style.'Index/customer');
    }
    
    /**
     * 名书视频、名师讲堂、交房杂项、常识知识
     */
    public function famousvideos(){
        $type = request()->get('type','');
        //var_dump($type);
        if($type == 1){
            $course_service = new CourseService();
            $subject_list = $course_service->getSubjectList(1,0,['subject_id'=>array('in','27,28,29')],'sort asc');
            $this->assign('subject_list', $subject_list['data']);
            if(request()->isAjax()){
                $course_service = new CourseService();
                $page_index = request()->post('page_index', 1);
                $page_size = request()->post('page_size', PAGESIZE);
                $condition['subject_id_1'] = request()->post('subject_id','');
                $course_list = $course_service->getCourseList($page_index, $page_size, $condition, 'sort asc');
                
                return $course_list;
            }
            return view($this->style . 'Index/famousVideos'); 
        }else if ($type ==2){
            $course_service = new CourseService();
            $subject_list = $course_service->getSubjectList(1,0,['subject_id'=>array('in','27,28,29')],'sort asc');
            $this->assign('subject_list', $subject_list['data']);
            if(request()->isAjax()){
            $course_service = new CourseService();
            $page_index = request()->post('page_index', 1);
            $page_size = request()->post('page_size', PAGESIZE);
            $condition['subject_id'] = request()->post('subject_id','');
            $teacher_list = $course_service->getTeacherList($page_index, $page_size, $condition, 'sort asc');
            return $teacher_list;
            }
            //查询分类
           
            $this->assign('type', request()->get('type','0'));
            return view($this->style . 'Index/famousTeachers');
        }else if ($type ==3){
            return view($this->style . 'Index/miscelLaneous');
        }elseif ($type == 4){
            
             $article = new Article();
            $subject_list = $article->getArticleClass(1,0,['class_id'=>array('in','1,2,3')],'sort asc');
            $this->assign('subject_list', $subject_list['data']);
            if(request()->isAjax()){
                $article = new Article();
                $page_index = request()->post('page_index', 1);
                $page_size = request()->post('page_size', PAGESIZE);
                $condition['ncac.class_id'] = request()->post('class_id','');
                $article_list = $article->getArticleList($page_index, $page_size, $condition, 'sort asc');
                return $article_list;
            }
            
            return view($this->style . 'Index/commonKnowledge');
        }else{
            return view($this->style . 'Index/famousVideos');
        }
    }
    
    /**
     * 名师视频下的列表页
     */
    public function famousvideosInfo(){
        return view($this->style.'Index/famousVideosInfo');
    }
    /**
     * 视频详情页
     */
    public function famousVideosDetail(){
        // 获取课程详情
         $course_data = new CourseService();
         $course_id = request()->get("course_id",'');
         $course = $course_data->getCourseInfo($course_id);
         $this->assign("course",$course);
         
         //获取讲师相资料
         foreach($course['course_teacher_list'] as $key=>$val)
         {
             $id_list[]=$val['teacher_id'];
         }
         $id_list=join(',',$id_list);
         $condition['id']=['in',$id_list];
         $teacher_list=$course_data->getTeacherList(1,0,$condition);
         $this->assign('teacher_list',$teacher_list['data']);
         
         // 获取课程章节列表
         $kpoint_list = $course_data->getCourseKpointList($course_id);
         $this->assign('kpoint_list',$kpoint_list);
         
        return view($this->style.'Index/famousVideosDetail');
    }
    /**
     * 常识详情
     */
    public function knowledgeDetail() {
        $article = new Article();
        $article_id = request()->get('article_id','');
        $article_detail = $article ->getArticleDetail($article_id);
        $this->assign('article_detail',$article_detail);
        return view($this->style.'Index/knowledgeDetail');
        
    }
    
    
    
   

    

    
    
    

    

    

   
    
}