<?php
namespace app\workswap\controller;

use data\service\Member;
use data\worksservice\Order as orderService;
use data\service\UnifyPay;
use data\worksservice\VmemberAccount;
/**
 * 订单
 * @author lzw
 *
 */
class Order extends BaseController
{

    public function __construct(){
        parent::__construct();
        $this->checkLogin();
    }
    
    /**
     * 订单确认
     */
    public function index(){
        
        return view($this->style . 'Order/index');
    }
    
    /**
     * 检测用户
     */
    private function checkLogin()
    {
        $uid = $this->uid;
        if (empty($uid)) {
            $redirect = __URL(__URL__ . "/login");
            $this->redirect($redirect); // 用户未登录
        }
        $is_member = $this->user->getSessionUserIsMember();
        if (empty($is_member)) {
            $redirect = __URL(__URL__ . "/login");
            $this->redirect($redirect); // 用户未登录
        }
    }
    /**
     * 订单确认支付
     */
    public function paymentOrder(){
        
        
        $member_service = new Member();
        //会员余额
        $member_balance = $member_service->memberShopBalanceCount($this->uid);
        $this->assign('member_balance', $member_balance);
        
        $order_money = 0;  //订单总金额
        $order_item = array(); //订单项
        if($_SESSION['order_tag'] == 'cart'){
            
        }elseif($_SESSION['order_tag'] == 'buy_now'){
            
            $order_relation_list = $_SESSION['order_relation_list'];

            $order_service = new orderService();
            $order_money = $order_service->getOrderRelationListMoney($order_relation_list);
            
            $order_item_list = $order_service->getOrderRelationItemList($order_relation_list);
            
        }
        $this->assign('order_item_list', $order_item_list);
        $this->assign('order_money', $order_money);
        
        //优惠劵列表
        $member_coupon = new VmemberAccount();
        $member_coupon_list = $member_coupon->getMemberCouponList(1,0, ['status'=>0]);
        $this->assign('member_coupon_list', $member_coupon_list['data']);
        
        return view($this->style . 'Order/paymentOrder');
    }
    
    /**
     * 订单创建
     */
    public function createOrder(){
        
        $order_data = $_POST['order_data'];
        $order_relation_list = $_SESSION['order_relation_list'];
        
        $order_service = new orderService();
        $res = $order_service->createOrder($order_data, $order_relation_list);
        return AjaxReturn($res);
    }
    
    /**
     * 下单成功后的页面
     */
    public function createOrderSuccess(){
        
        $out_trade_no = request()->get('no', '');
        
        $order_service = new orderService();
//         if(!empty($out_trade_no)){
//             $out_trade_no_new = $order_service->createOutTradeNo();
          
//         }
        
        $this->assign('out_trade_no', $out_trade_no);
        $v_order_info = $order_service->getOrderInfo(0, ['out_trade_no'=>$out_trade_no],'*');

        $this->assign('v_order_info', $v_order_info);
   
        
        $member_service = new Member();
        //会员余额
        $member_balance = $member_service->memberShopBalanceCount($this->uid);
        $this->assign('member_balance', $member_balance);

        return view($this->style . 'Order/createOrderSuccess');
    }
    
    /**
     * 余额支付订单
     */
    public function ajaxBalancePayOrder(){
        
        if(request()->isAjax()){
            
            $out_trade_no = request()->post('out_trade_no', '');
            $v_order_service = new orderService();
            $res = $v_order_service->orderOnLinePay($out_trade_no, 3);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 订单session值创建
     */
    public function orderCreateSession(){
    
        $tag = request()->post('tag', '');
        if (empty($tag)) {
            return - 1;
        }
        if ($tag == 'cart') {
            $_SESSION['order_tag'] = 'cart';
            $_SESSION['cart_list'] = request()->post('cart_id');
        }
        if ($tag == 'buy_now') {
            $_SESSION['order_tag'] = 'buy_now';
            $relation_list[] = array(
                'relation_id' => request()->post('relation_id'),
                'relation_type' => request()->post('relation_type'),
                "kpoint_id"     =>request()->post("kpoint_id", ""),
            );

            $_SESSION['order_relation_list'] = json_encode($relation_list);
        }
        return 1;
    }
    
    /**
     * 根据会员拥有的优惠劵和订单金额计算可优惠金额
     */
    public function setOrderCouponMoney(){
        
        $mc_id = request()->post('mc_id', '');
        $order_money = request()->post('order_money', '');
        
        $order_service = new orderService();
        $order_money = $order_service->setOrderCouponReplaceMoney($mc_id, $order_money);

        if($order_money > 0){
            return array('code'=>$order_money);
        }else {
            return AjaxReturn($order_money);
        }
    }
    
    /**
     * 订单详情
     */
    public function OrderInfo(){
        
        $out_trade_no = request()->get('out_trade_no', '');
        $order_service = new orderService();
        
        //订单详情
        $v_order_info = $order_service->getOrderInfo(0, ['out_trade_no'=>$out_trade_no]);
        $this->assign('v_order_info', $v_order_info);

        //优惠劵
        $mc_id = $v_order_info['mc_id'];
        $vmember_account = new VmemberAccount();
        $member_coupon_info = $vmember_account->getMemberCouponInfo($mc_id);
        $this->assign('member_coupon_info', $member_coupon_info);
        
        //订单项列表
        $v_order_item = $order_service->getOrderItemList($v_order_info['v_order_id']);
        
        $this->assign('order_item_list', $v_order_item);
        
        return view($this->style . 'Order/OrderInfo');
    }
}