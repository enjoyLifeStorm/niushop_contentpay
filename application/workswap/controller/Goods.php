<?php
namespace app\workswap\controller;
use data\worksservice\Goods as GoodsService;
use data\worksservice\yiwentang;


class Goods extends BaseController
{
    private $goods_service = null;
    public function __construct(){
        parent::__construct();
        $this->goods_service = new GoodsService();
    }
    
    /**
     * 商品列表
     */
    public function Onlinemall(){
        
        $goods_data = new goodsService();
        
        if(request()->isAjax()){
            $goods_data = new goodsService();
            $search = request()->post('search','');
            $class_id = request()->post('class_id');
            $condition['goods_name'] = [
                'like',
                '%' . $search . '%'
            ];
            if(!empty($class_id)) $condition['class_id'] = $class_id;
             
            $goods_list = $goods_data->getGoodsList(1,0,$condition,'');
            return $goods_list;
        }
        
        $goods = new GoodsService();
        $list = $goods_data->getGoodsClassList(1, 0, ['pid'=>0], 'sort asc');
        $this->assign('list', $list['data']);
        
        return view($this->style . 'Goods/index');
    }
    
    /**
     * 商品详情
     */
    public function goodsInfo(){
        $goods_id = request()->get('goods_id','');
        $goods_data = new goodsService();
        $goods_info = $goods_data->getGoodsInfo($goods_id);
        //var_dump($goods_info);
        $this->assign('goods_info',$goods_info);
        
        // 客服信息
        $yiwentang = new yiwentang();
        $kefu_info = $yiwentang->getYwtKefu();
        $kefu_info = json_decode($kefu_info,true);
        $this->assign('kefu_info',$kefu_info);
        return view($this->style . 'Goods/goodsInfo');
    }
    
    
    
    
    
    
    
    
    
    /**
     * 课程列表页
     */
    public function index(){
        
        $condition = ['subject_id'=>array('in','27,28,29')];    //暂时性的，如正式发布将条件清除
        $subject_list = $this->course_service->getSubjectList(1,0, $condition,'sort asc');
        $this->assign('subject_list', $subject_list['data']);
        
        $this->assign('head_title', '书画视频');
        return view($this->style . 'Course/index');
    }
    
    /**
     * 课程列表具体分页数据
     */
    public function controlCourseList(){
        
        $condition['subject_id_1'] = request()->post('subject_id','27');
        $course_list = $this->course_service->getCourseList(1, 0, $condition, 'sort asc');
        $this->assign('course_list', $course_list['data']);
        return view($this->style . 'Course/controlCourseList');
    }
    
    /**
     * 视频详情页
     */
    public function courseInfo(){
        
        //检测视频的拥有
        $this->isHaveWorks();
        
        // 获取课程详情
        $course_data = new CourseService();
        $course_id = request()->get("course_id",'');
        $this->assign('course_id', $course_id);
        
        $course = $course_data->getCourseInfo($course_id);
        $this->assign("course",$course);
         
        //获取讲师相资料
        foreach($course['course_teacher_list'] as $key=>$val)
        {
            $id_list[]=$val['teacher_id'];
        }
        $id_list=join(',',$id_list);
        $condition['id']=['in',$id_list];
        $teacher_list=$course_data->getTeacherList(1,0,$condition);
        $this->assign('teacher_list',$teacher_list['data']);
         
        // 获取课程章节列表
        $kpoint_list = $course_data->getCourseKpointList($course_id);
  
        $this->assign('kpoint_list',$kpoint_list);
         //获取该课程是否被收藏
         $course_favourse = new VmemberAccount();
         $relation_id = $course_id;
         $fav_info = $course_favourse->isCourseCollection($relation_id);
         $this->assign('fav_info',$fav_info);
         //var_dump($fav_info);
        return view($this->style.'Course/courseInfo');
    }
    
    /**
     * 是否拥有课程
     */
    public function isHaveWorks(){

        $member = new MemberService();
        $member_info = $member->getMemberDetail($this->instance_id);
        
        $is_login = 0;
        //会员是否登录
        if(!empty($member_info)){
            $is_login = 1;
        }
        $this->assign('is_login', $is_login);
        $course_id = request()->get('course_id', '');
        $vmember_service = new Vmember();

        //会员是否拥有该课程
        $is_have_course = $vmember_service->getVmemberHaveIsCourse($course_id);
        $this->assign('is_have_course', $is_have_course);
    }
    /**
     * 站内搜索
     */
    public function SiteSearch(){
        if(request()->isAjax()){
            $course_data = new CourseService();
            $search = request()->post('search','');
            $condition['course_title'] = [
                'like',
                '%' . $search . '%'
            ];
            $course_list = $course_data->getCourseList(1, 0, $condition, 'sort asc');
            return $course_list;
        }
        return view($this->style.'Course/SiteSearch');
    }
    
    
}