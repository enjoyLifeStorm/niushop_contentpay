<?php
namespace app\workswap\controller;
use data\service\Article as ArticleService;
use data\worksservice\VmemberAccount;
use data\service\Member as MemberService;

/**
 * 文章控制器（暂当常识知识文章）
 * @author lzw
 *
 */
class Article extends BaseController
{

    public function __construct(){
        parent::__construct();
        
    }
    
    /**
     * 常识知识首页
     */
    public function index(){
        $article = new ArticleService();
        $subject_list = $article->getArticleClass(1,0,['class_id'=>array('in','1,2,3')],'sort asc');
        $this->assign('subject_list', $subject_list['data']);
        return view($this->style . 'Article/index');
    }
    
    /**
     * 文章列表信息
     */
    public function articleList(){
        
        if(request()->isAjax()){
            
            $article = new ArticleService();
            
            $page_index = request()->post('page_index', 1);
            $page_size = request()->post('page_size', PAGESIZE);
            $class_id = request()->post('class_id');
            $condition = [];
            if(!empty($class_id)) $condition['ncac.class_id'] = $class_id;
           
            $article_list = $article->getArticleList($page_index, $page_size, $condition, 'sort asc');
            
            $this->assign('article_list', $article_list['data']);
            
            return view($this->style . 'Article/controlArticleList');
        }
    }
    
    /**
     * 文章详情页
     */
    public function articleInfo(){
        $article = new ArticleService();
        $article_id = request()->get('article_id','');
        $article_detail = $article ->getArticleDetail($article_id);
        $article_detail['creat_time'] = date("Y-m-d H:i:s",$article_detail['create_time']);
        $this->assign('article_detail',$article_detail);
        
        //评论列表
        $account = new VmemberAccount;
        $condition_e['relation_id'] = $article_id;
        $condition_e['relation_type'] = 4;
        $condition_e['is_show'] = 1;
        $data = $account->getMemberEvaluateList(1,0,$condition_e,'evaluate_id desc');
        $evaluate_list = $data['data'];
        if(empty($evaluate_list)) $evaluate_list = [];
        $this->assign('evaluate_list',$evaluate_list);
        
        //用户信息
        $member = new MemberService();
        $member_info = $member->getMemberDetail($this->instance_id);
        if(!$member_info){
            $member_info = [];
        }
        $this->assign('member_info',$member_info);
        
        return view($this->style.'Article/articleInfo');
    }
}