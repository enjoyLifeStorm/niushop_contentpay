<?php
namespace app\workswap\controller;
use data\worksservice\Course as CourseService;
/**
 * 文章控制器（暂当常识知识文章）
 * @author lzw
 *
 */
class Teacher extends BaseController
{

    public function __construct(){
        parent::__construct();
        
    }
    
    /**
     * 常识知识首页
     */
    public function index(){
        
        $course_service = new CourseService();
        
        $condition['is_visible'] = 1;
        $condition['parent_id'] = 0;
        $subject_list = $course_service->getSubjectList(1,0, $condition,'sort asc');
        
        foreach($subject_list['data'] as $key=>$val){
            $condition['parent_id'] = $val['subject_id'];
            $child_list = $course_service->getSubjectList(1,0, $condition,'sort asc');
            $val['child_list'] = $child_list['data'];
        }
        $this->assign('subject_list', $subject_list['data']);
        
        return view($this->style . 'Teacher/index');
    }
    
    /**
     * 讲师列表信息
     */
    public function controlTeacherList(){
        
            $course_service = new CourseService();
            $condition = []; 
            
            $subject_id_1 = request()->post('subject_id_1');
            $subject_id_2 = request()->post('subject_id_2');
            
            if(!empty($subject_id_1)) $condition['subject_id_1'] = $subject_id_1;
            if(!empty($subject_id_2)) $condition['subject_id_2'] = $subject_id_2;
            
            $teacher_list = $course_service->getTeacherList(1, 0, $condition, 'ns.sort asc');
            
            if(empty($teacher_list['data'])) return 0;
            
            $this->assign('teacher_list',$teacher_list['data']);
            
            return view($this->style . 'Teacher/controlTeacherList');
    }
    
    /**
     * 讲师详情页
     */
    public function TeacherInfo(){
        
        $teacher_id = request()->get('teacher_id','');
        $course_service = new CourseService();
        
        $teacher_info = $course_service->getTeacherInfo($teacher_id);
        $this->assign('teacher_info',$teacher_info);
        
        //获取教师的相关课程
        $course_list = $course_service->getTeacherByCourse($teacher_id);
        
        $this->assign('course_list',$course_list);
        
        return view($this->style.'Teacher/TeacherInfo');
    }
}