<?php
namespace app\workspc\controller;
use data\service\Article as ArticleService;

/**
 * 资讯
 * @author lzw
 *
 */
class Article extends BaseController
{

    public function __construct(){
        parent::__construct();
        
    }
    /**
     * 资讯首页 
     */
    public function articleList(){
        
        $article = new ArticleService;
        
        // 咨询列表
        $page = request()->get('page', 1);
        $page_size = 8;
        //$condition['ncac.class_id'] = request()->get('class_id','');
        $article_list = $article->getArticleList($page, $page_size, '', 'sort asc');
        $this->assign("article_list",$article_list['data']);
        
        $this->assign('page_count',$article_list['page_count']);
        $this->assign('page',$page);
        
        // 咨询排行榜 咨询推荐
        $hot_article_list = $article->getArticleList(1, 10, '', 'sort asc');
        $this->assign('hot_article_list',$hot_article_list['data']);
        
        return view($this->style . 'Article/articleList');
    }
    
    /**
     * 资讯文章详情
     */
    public function articleInfo(){
        $article_id = request()->get('article_id');
        $article = new ArticleService;
        $article_info = $article->getArticleDetail($article_id);
        $this->assign('article_info',$article_info);
        
        // 咨询排行榜 咨询推荐
        $hot_article_list = $article->getArticleList(1, 10, '', 'sort asc');
        $this->assign('hot_article_list',$hot_article_list['data']);
        
        return view($this->style . 'Article/articleInfo');
    }
}