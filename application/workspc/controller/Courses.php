<?php
namespace app\workspc\controller;

use data\worksservice\Course as lession;


/**
 * 
 * 录播视频页控制器
 */
class Courses extends BaseController
{
    public $course;
    
    public function __construct()
    {
        $this->course = new lession();
        parent::__construct();
    } 
    /** 
     * 
     * 录播视频列表页 
     */
    public function courseList(){
        
        //获取分类数据
        $condition['is_visible']=1;
        $condition['parent_id']=0;
        $subject_list = $this->course->getSubjectList(1,0,$condition);
        $this->assign('subject_list', $subject_list['data']);
        
        //获取下级分类
        $condition['parent_id']=request()->get('subject_id','');
        $subject_child_list = $this->course->getSubjectList(1,0,$condition);
        $this->assign('subject_child_list', $subject_child_list['data']);
        
        //获取课程分页数据
        $pagesize=4;
        
        $page = request()->get('page','');
        if(!$page){
            $page=1;
        }
        
        $condition1['status']=array('in','1,2');
        
        if(request()->get('subject_id','')){
            if(request()->get('subject_child_id','')){
                $condition1['subject_id'] = request()->get('subject_child_id','');
            }else{
                if($subject_child_list['data']){
                    foreach($subject_child_list['data'] as $key=>$val)
                    {
                        $id_list[]=$val['subject_id'];
                    }
                    $id_list = join(',',$id_list);
                    $condition1['subject_id']=[['eq',request()->get('subject_id','')],['in',$id_list],'or'];
                }else{
                    $condition1['subject_id'] = request()->get('subject_id','');
                }        
            }     
        }
        
        if(request()->get('is_free','')=="ok"){
            $condition1['current_price']=0;
        }
        
        $order = request()->get('order','');
        if(!$order){
            $order="sort";
        }
        
        $sort_type = request()->get('sort_type','');
        if(!$sort_type){
            $sort_type="asc";
        }
        
        $course_list=$this->course->getCourseList($page,$pagesize,$condition1,"$order $sort_type");
        
        $this->assign('course_list',$course_list['data']);
        
        $data=request()->get();
        $this->assign('data',$data);
        
        // 公共分页数据
        $this->assign('page_count', $course_list['page_count']);
        $this->assign('page', $page); 
        
        return view($this->style . 'Courses/courseList');
    }
    /**
     *
     * 录播视频详情页
     */
     public function courseDetail(){
         
         // 获取课程详情
         $course_id = request()->get("course_id",'');
         $course = $this->course->getCourseInfo($course_id);
         $this->assign("course",$course);
         
         //获取讲师相资料
         foreach($course['course_teacher_list'] as $key=>$val)
         {
             $id_list[]=$val['teacher_id'];
         }
         $id_list=join(',',$id_list);
         $condition['id']=['in',$id_list];
         $teacher_list=$this->course->getTeacherList(1,0,$condition);
         $this->assign('teacher_list',$teacher_list['data']);
         
         // 获取课程章节列表
         $kpoint_list = $this->course->getCourseKpointList($course_id);
         $this->assign('kpoint_list',$kpoint_list);
         
         //获取推荐课程列表
         $course_list=$this->course->getCourseList(1,3,['status'=>['in','1,2']],'modify_time desc');
         $this->assign("course_list",$course_list['data']);
         
         return view($this->style . 'Courses/courseDetail');
     }
    
}
