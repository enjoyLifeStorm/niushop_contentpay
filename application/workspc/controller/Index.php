<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\workspc\controller;

use data\worksservice\Website;
use data\worksservice\Course;
use data\service\Platform;
use data\service\Article;
/**
 * 首页控制器
 * 创建人：王永杰
 * 创建时间：2017年2月6日 11:01:19
 */
class Index extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function _empty($name)
    {}

    /**
     * 首页
     */
    public function index()
    {
        $default_client = request()->cookie("default_client", "");
        $web_info = $this->web_site->getWebSiteInfo();
        if ($default_client == "shop") {} elseif (request()->isMobile()&&$web_info['wap_status'] != 2) {
            $redirect = __URL(__URL__ . "");
            $this->redirect($redirect);
            exit();
        }
        // 首页板块
        $website_service = new Website();
        $home_plate_list = $website_service->getHomePlateList(['plate_type'=>1]);
        $this->assign('home_plate_list', $home_plate_list);
        
        // 获取分类列表
        $course = new Course();
        $data = $course->getSubjectList(1,7,['is_visible'=>1,'parent_id'=>0],'sort asc');
        $subject_list = $data['data'];
        
        foreach($subject_list as $key=>$val){
            $subject_child_list = $course->getSubjectList(1,0,['is_visible'=>1,'parent_id'=>$val['subject_id']],'sort asc');
            $val['subject_child_list'] = $subject_child_list['data'];
        }
        $this->assign('menu',$subject_list);
        
        // 首页包含菜单控制变量
        $index_menu_cotroller = 1;
        $this->assign('index_menu_cotroller',$index_menu_cotroller);
        
        // 轮播图
        $website_service = new Website();
        $adv_list = $website_service->getWebsiteAdvList(1,0,['ap_id'=>11]);
        $this->assign('adv_list',$adv_list['data']);
        

        //专业楼层显示
        $subject_home_block = $website_service->getSubjectHomeBlock();
        $this->assign('subject_home_block', $subject_home_block);
        //dump($subject_home_block[0]);
        
        //名师
        $teacher_list = $course->getTeacherList(1,4);
        $this->assign('teacher_list', $teacher_list['data']);
        
        // 文章列表
        $article = new Article();
        $article_list = $article->getArticleList(1, 11, ["status" => 2], 'sort asc');
        $this->assign("article_list", $article_list['data']);
        return view($this->style . 'Index/index');
        
    }
    
    


    
}
