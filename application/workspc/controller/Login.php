<?php
/**
 * Login.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\workspc\controller;

use data\extend\ThinkOauth as ThinkOauth;
use data\service\Config as Config;
use data\service\Member as Member;
use data\service\WebSite as WebSite;
use think\Controller;
use think\Session;
use think\Log;
\think\Loader::addNamespace('data', 'data/');

/**
 * 登录控制器
 * 创建人：李吉
 * 创建时间：2017-02-06 10:59:23
 */
class Login extends Controller
{
    // 验证码配置
    public $login_verify_code;
    // 通知配置
    public $notice;

    public function __construct()
    {
        $default_client = request()->cookie("default_client", "");
        if ($default_client == "shop") {} elseif (request()->isMobile()) {
            $redirect = __URL(__URL__ . "/wap");
            $this->redirect($redirect);
            exit();
        }
        
        // 当切换到PC端时，隐藏右下角返回手机端按钮
        if (! request()->isMobile() && $default_client == "shop") {
            $default_client = "";
        }
        parent::__construct();
        $this->init();
    }

    public function _empty($name)
    {}

    public function init()
    {
        $this->user = new Member();
        $this->web_site = new WebSite();
        $config = new Config();
        $web_info = $this->web_site->getWebSiteInfo();
        if ($web_info['web_status'] == 2) {
            webClose($web_info['close_reason']);
        }
        $this->assign("platform_shopname", $this->user->getInstanceName());
        $this->assign("title", $web_info['title']);
        
        // 获取当前使用的PC端模板
        $this->instance_id = 0;
        $use_pc_template = $config->getUsePCTemplate($this->instance_id);
        
        if (empty($use_pc_template)) {
            $use_pc_template['value'] = 'default';
        }
//         if (! checkTemplateIsExists("shop", $use_pc_template['value'])) {
//             $this->error("模板配置有误，请联系商城管理员");
//         }
        $this->style = "shop/" . $use_pc_template['value'] . "/";
        $this->assign("style", "shop/" . $use_pc_template['value']);
        
        // 是否开启qq跟微信
        $instance_id = 0;
        $qq_info = $config->getQQConfig($instance_id);
        $Wchat_info = $config->getWchatConfig($instance_id);
        $this->assign("qq_info", $qq_info);
        $this->assign("Wchat_info", $Wchat_info);
        // 是否开启验证码
        $this->login_verify_code = $config->getLoginVerifyCodeConfig($instance_id);
        $this->assign("login_verify_code", $this->login_verify_code["value"]);
        // 是否开启通知
        $noticeMobile = $config->getNoticeMobileConfig($instance_id);
        $noticeEmail = $config->getNoticeEmailConfig($instance_id);
        $this->notice['noticeEmail'] = $noticeEmail[0]['is_use'];
        $this->notice['noticeMobile'] = $noticeMobile[0]['is_use'];
        $this->assign("notice", $this->notice);
        
        // 配置头部
        $seoconfig = $config->getSeoConfig($instance_id);
        $this->assign("seoconfig", $seoconfig);
    }



    /*
     * 吴奇
     * 首页登录
     * 2017/3/7 9:55
     */
    public function login()
    {
        $tencent = new Config();
        $instance_id = 0;
        $qq_info = $tencent->getQQConfig($instance_id);
        $Wchat_info = $tencent->getWchatConfig($instance_id);
        $this->assign("qq_info", $qq_info);
        $this->assign("Wchat_info", $Wchat_info);
        
        $username = request()->post('username', '');
        $password = request()->post('password', '');
        if ($this->login_verify_code["value"]["pc"] == 1) {
            $vertification = request()->post('vertification', '');
            if (! captcha_check($vertification)) {
                
                $retval = [
                    'code' => 0,
                    'message' => "验证码错误"
                ];
                return $retval;
            }
        }
        
        
        $res = $this->user->login($username, $password);
       
        if ($res == 1) {
            if (! empty($_SESSION['login_pre_url'])) {
                $retval = [
                    'code' => 1,
                    'url' => $_SESSION['login_pre_url']
                ];
            } else {
                $retval = [
                    'code' => 2,
                    'url' => 'index/index'
                ];
            }
        } else {
            $retval = AjaxReturn($res);
        }
        return $retval;
    }


    public function register()
    {
        if (request()->isAjax()) {
            // 获取数据库中的用户列表
            $username = request()->get('username', '');
            $exist = $this->user->judgeUserNameIsExistence($username);
            return $exist;
        }
        if (request()->isPost()) {
            $min = 10000000000;
            $max = 19999999999;
            $member = new Member();
            $password = request()->post('user_password', '');
            // $email = rand($min, $max) . '@qq.com';
            // $mobile = rand($min, $max);
            $user_name = request()->post("user_name", '');
            
            $retval = $member->registerMember($user_name, $password, '', '', '', '', '', '', '');
            dump($retval);
            
            if ($retval > 0) {
                // $this->success("注册成功", __URL__."/index");
                $redirect = __URL("WORKS_MAIN/index/index");
                $this->redirect($redirect);
            } else {
                $error_array = AjaxReturn($retval);
                $message = $error_array["message"];
                $redirect = __URL("WORKS_MAIN/index/index");
                $this->error($message, $redirect);
                return AjaxReturn($retval);
            }
        }
        $instanceid = 0;
        $config = new Config();
        // 验证注册配置
        $this->verifyRegConfig("plain");
        $website = new WebSite();
        $web_info = $website->getWebSiteInfo();
        $phone_info = $config->getMobileMessage($instanceid);
        $this->assign("phone_info", $phone_info);
        
        $email_info = $config->getEmailMessage($instanceid);
        $this->assign("email_info", $email_info);
        $this->assign("web_info", $web_info);
        $this->assign("title_before","普通注册");
        return view($this->style . 'Login/register');
    }

    /**
     * 验证码
     *
     * @return multitype:number string
     */
    public function verify()
    {
        $vertification = request()->post('vertification', '');
        if (! captcha_check($vertification)) {
            $retval = [
                'code' => 0,
                'message' => "验证码错误"
            ];
        } else {
            $retval = [
                'code' => 1,
                'message' => "验证码正确"
            ];
        }
        return $retval;
    }


    /**
     *
     * @return Ambigous <\think\response\View, \think\response\$this, \think\response\View>
     */
    public function check_code()
    {
        $send_param = request()->post('send_param', '');
        $param = Session::get('emailVerificationCode');
        if ($send_param == $param && $send_param != '') {
            $retval = [
                'code' => 0,
                'message' => "验证码一致"
            ];
        } else {
            $retval = [
                'code' => 1,
                'message' => "验证码不一致"
            ];
        }
        return $retval;
    }

    public function ckeck_find_password_code()
    {
        $send_param = request()->post('send_param', '');
        $param = Session::get('forgotPasswordVerificationCode');
        if ($send_param == $param && $send_param != '') {
            $retval = [
                'code' => 0,
                'message' => "验证码一致"
            ];
        } else {
            $retval = [
                'code' => 1,
                'message' => "验证码不一致"
            ];
        }
        return $retval;
    }

   

    
    
   
}