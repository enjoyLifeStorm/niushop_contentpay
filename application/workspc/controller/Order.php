<?php
namespace app\workspc\controller;

use data\service\Member;
use data\worksservice\Order as orderService;
use data\worksservice\VmemberAccount;
/**
 * 订单
 * @author lzw
 *
 */
class Order extends BaseController
{

    public function __construct(){
        parent::__construct();
        
    }
    
    /**
     * 订单确认
     */
    public function index(){
        
        return view($this->style . 'Order/index');
    }
    
    /**
     * 订单确认支付
     */
    public function paymentOrder(){
        
        // 定义一个将打折数字转化为文字的方法
        function alter($num){
            $arr = ['零','一','二','三','四','五','六','七','八','九'];
            $num = (int)$num;
            if($num%10==0){
                $res = $arr[$num/10];
            }else{
                $n1 = floor($num/10);
                $n2 = $num%10;
                $res = $arr[$n1].$arr[$n2];
            }
            return $res;
        }
        
        $member_service = new Member();
        //会员余额
        $member_balance = $member_service->memberShopBalanceCount($this->uid);
        if(!$member_balance) $member_balance = '0.00';
        $this->assign('member_balance', $member_balance);
        
        $order_money = 0;  //订单总金额
        $order_item = array(); //订单项
        if($_SESSION['order_tag'] == 'cart'){
            
        }elseif($_SESSION['order_tag'] == 'buy_now'){
            
            $order_relation_list = $_SESSION['order_relation_list'];

            $order_service = new orderService();
            $order_money = $order_service->getOrderRelationListMoney($order_relation_list);
            
            $order_item_list = $order_service->getOrderRelationItemList($order_relation_list);
            
            // 获取我的优惠券列表
            $member_account = new VmemberAccount();
            $data = $member_account->getMemberCouponList(1,0,['status' => 0],'create_time desc');
            $my_coupon_list = $data['data'];
            
            // 如果是打折类型的优惠券则将数字转换为文字
            foreach($my_coupon_list as $key=>$val){
                if($val['discount_type']==2){
                    $val['type_value_1'] = alter($val['type_value']).'折';
                }
            }
            
        }
        
        //dump($order_relation_list);
        //exit;
        $this->assign("type" , json_decode($order_relation_list,true)[0]['relation_type']);
        $this->assign('order_item_list', $order_item_list);
        $this->assign('order_money', $order_money);
        $this->assign('my_coupon_list',$my_coupon_list);
        
        return view($this->style . 'Order/paymentOrder');
    }
    
    /**
     * 订单创建
     */
    public function createOrder(){
        
        $order_data = $_POST['order_data'];
        $order_relation_list = $_SESSION['order_relation_list'];
        
        $order_service = new orderService();
        $res = $order_service->createOrder($order_data, $order_relation_list);
        return AjaxReturn($res);
    }
    
    /**
     * 下单成功后的页面
     */
    public function createOrderSuccess(){
    
        $out_trade_no = request()->get('no', '');
        $this->assign('out_trade_no', $out_trade_no);
    
        $order_service = new orderService();
        $v_order_info = $order_service->getOrderInfo(0, ['out_trade_no'=>$out_trade_no],'*');
        $this->assign('v_order_info', $v_order_info);
        
    
        $member_service = new Member();
        //会员余额
        $member_balance = $member_service->memberShopBalanceCount($this->uid);
        $this->assign('member_balance', $member_balance);
        
    
        return view($this->style . 'Order/createOrderSuccess');
    }
    
    /**
     * 订单session值创建
     */
    public function orderCreateSession(){
    
        $tag = request()->post('tag', '');
        if (empty($tag)) {
            return - 1;
        }
        if ($tag == 'cart') {
            $_SESSION['order_tag'] = 'cart';
            $_SESSION['cart_list'] = request()->post('cart_id');
        }
        if ($tag == 'buy_now') {
            $_SESSION['order_tag'] = 'buy_now';
            $relation_list[] = array(
                'relation_id' => request()->post('relation_id'),
                'relation_type' => request()->post('relation_type'),
                "kpoint_id"     =>request()->post("kpoint_id", ""),
            );

            $_SESSION['order_relation_list'] = json_encode($relation_list);
        }
        return 1;
    }
    
    /**
     * 余额支付订单
     */
    public function ajaxBalancePayOrder(){
    
        if(request()->isAjax()){
    
            $out_trade_no = request()->post('out_trade_no', '');
    
            $v_order_service = new orderService();
    
            $res = $v_order_service->orderOnLinePay($out_trade_no, 3);
    
            return AjaxReturn($res);
        }
    }
    
}