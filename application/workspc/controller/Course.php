<?php
namespace app\workspc\controller;

use data\worksservice\Course as lession;
use data\worksservice\Vmember;
use data\worksservice\VmemberAccount;
use data\service\Member as MemberService;

/**
 * 
 * 录播视频页控制器
 */
class Course extends BaseController
{
    public $course;
    
    public function __construct()
    {
        $this->course = new lession();
        parent::__construct();
    } 
    /** 
     * 
     * 录播视频列表页 
     */
    public function courseList(){
        
        //获取分类数据
        $condition['is_visible']=1;
        $condition['parent_id']=0;
        $subject_list = $this->course->getSubjectList(1,0,$condition,'sort asc');
        $this->assign('subject_list', $subject_list['data']);
        
        //获取下级分类
        $condition2['is_visible']=1;
        $condition2['parent_id']=request()->get('sid1');
        $subject_child_list = $this->course->getSubjectList(1,0,$condition2,'sort asc');
        $this->assign('subject_child_list', $subject_child_list['data']);
        
        //获取课程分页数据
        $pagesize=4;
        
        $page = request()->get('page',1);
        
        $condition1['status']=array('in','1,2');
        
        if(request()->get('sid1')){
            if(request()->get('sid2')){
                $condition1['subject_id_2'] = request()->get('sid2');
            }else{
                $condition1['subject_id_1'] = request()->get('sid1');    
            }     
        }
        
        if(request()->get('is_free')=="ok"){
            $condition1['current_price']=0;
        }
        
        $order = request()->get('order','sort');
        if($order=='buy_count'){
            $order = 'buy_count+basics_buy';
        }
        
        if(request()->get('key')){
            $condition1['course_title']=['like','%'.request()->get('key').'%'];
        }
        
        $sort_type = request()->get('sort_type','asc');
        
        
        $course_list=$this->course->getCourseList($page,$pagesize,$condition1,"$order $sort_type");
        
        $this->assign('course_list',$course_list['data']); 
        
        // 公共分页数据
        $this->assign('page_count', $course_list['page_count']);
        $this->assign('page', $page); 
        
        // 返回模板做默认
        $data=request()->get();
        $this->assign('data',$data);
        
        return view($this->style . 'Course/courseList');
    }
    
    /**
     *
     * 录播视频详情页
     */
     public function courseDetail(){
         
         // 获取课程详情
         $course_id = request()->get("course_id",'');
         $course = $this->course->getCourseInfo($course_id);
         $this->assign("course",$course);
         
         // 获取讲师关资料
         $teacher_list = [];
         
         if(!empty($course['course_teacher_list'])){
             foreach($course['course_teacher_list'] as $key=>$val)
             {
                 $id_list[]=$val['teacher_id'];
             }
              
             $id_list=join(',',$id_list);
             $condition['id']=['in',$id_list];
              
             $teacher_list=$this->course->getTeacherList(1,0,$condition)['data'];
         }
         
         $this->assign('teacher_list',$teacher_list);
         
         // 获取课程章节列表
         $kpoint_list = $this->course->getCourseKpointList($course_id);
         $this->assign('kpoint_list',$kpoint_list);
         
         //获取推荐课程列表
         $course_list=$this->course->getCourseList(1,3,['status'=>['in','1,2']],'modify_time desc');
         $this->assign("course_list",$course_list['data']);
         
         // 查询会员是否已购买课程或课程章节
         $vmember_service = new Vmember();
         
         $condition_h['uid'] = $this->uid;
         $condition_h['relation_type'] = 1;
         $condition_h['relation_id'] = $course_id;
         $have_info = $vmember_service->getVmemberHaveInfo('' , $condition_h);
         
         // 初始化相关数据
         $have_arr = [];//已购买章节数组
         $is_have_course = 0;//是否已拥有整个课程的
         $action = 0;// 控制评论与回复
         $free_count = 0;//免费章节数统计
         $free_arr = [];
         $start_key = 0;
         
         // 计算总共的章节个数
         $kpoint_count = count($kpoint_list);
         
         // 计算已购买章节的个数
         if(!empty($have_info['kpoint_id'])) $have_arr = explode(',', $have_info['kpoint_id']);
         $have_count = count($have_arr);
         
         // 计算免费章节的个数
         foreach($kpoint_list as $key=>$val){
              
             if($val['kpoint_price'] == 0.00){
                 $free_count++;
                 $free_arr[] = $val['kpoint_id'];
             }
         }
         
         if(!empty($have_info)){
         
             if(empty($have_info['kpoint_id'])){
         
                 $is_have_course = 1;
             }else{
                 // 如果已购买和免费的章节之和等于课程章节的总数 则表示客户已拥有了整个课程 将立即购买改为立即观看
                 if($have_count + $free_count == $kpoint_count) $is_have_course = 1;
             }
         }
         
         // 如果免费章节数或者已购买章节数不为零 则可以评论和回复和观看
         if($free_count > 0 || $have_count > 0) $action = 1;
         
         $this->assign('action', $action);
         $this->assign("is_have_course", $is_have_course);
         $this->assign("have_arr", $have_arr);
         
         
         // 获取会员资料
         $member = new MemberService();
         $member_info = $member->getMemberDetail($this->instance_id);
         if(!$member_info){
             $member_info = [];
         }
         $this->assign('member_info', $member_info);
         
         // 评论列表
         $account = new VmemberAccount;
         $condition_e['relation_id'] = $course_id;
         $condition_e['relation_type'] = 1;
         $condition_e['is_show'] = 1;
         $condition_e['reply_evaluate'] = 0;
         
         $data = $account->getMemberEvaluateList(1,0,$condition_e,'evaluate_id desc');
         
         $evaluate_list = $data['data'];
         
         foreach($evaluate_list as $key=>$val){
             $condition_r['relation_id'] = $course_id;
             $condition_r['relation_type'] = 1;
             $condition_r['is_show'] = 1;
             $condition_r['reply_evaluate'] = $val['evaluate_id'];
             $val['reply_list'] = $account-> getMemberEvaluateList(1,0,$condition_r,"evaluate_id desc")['data'];
             $val['praise_count'] = $account->setMemberDianzanCount(1, $val['evaluate_id']);
             foreach($val['reply_list'] as $ke=>$va){
                 $va['praise_count'] = $account->setMemberDianzanCount(1, $va['evaluate_id']);
             }
         }
         
         $this->assign('evaluate_list',$evaluate_list);
         
         // 获取该课程是否被收藏
         $account = new VmemberAccount();
         $is_collection = $account->isCourseCollection($course_id , 1);
         $this->assign('is_collection',$is_collection);
        
         //查看是否属于某一套餐及获取该套餐的详情
         $is_belong_package = $this->course->getPackageCourseList('',['course_id' => $course_id]);
         if($is_belong_package)
         {
             foreach($is_belong_package as $key=>$val)
             {
                 $package_info = $this->course->getPackageInfo($val['package_id']);
                 $package_course_list = $this->course->getPackageCourseList($val['package_id']);
                 $package_list[$key][0] = $package_info;
                 $package_list[$key][1] = $package_course_list;
             }
             
         }
         
         $this->assign('package_list' , $package_list);
        
         
         return view($this->style . 'Course/courseDetail');
     }
     
     /**
     * 创建人：赵海雷 
     * 添加评论 
     */
     public function addComment(){
         
         $evaluate_text = request()->post('content');
         $relation_id = request()->post('course_id');
         $reply_evaluate = request()->post('evaluate_id');
         $relation_type = request()->post('evaluate_type');

         $account = new VmemberAccount();
         $res = $account->addMemberEvaluate($relation_type, $relation_id, $evaluate_text,$reply_evaluate);
         $time = date('Y/m/d H:i');
         $data = [$res,$time];
         
         return json_encode($data);
         
     }
     
     /**
      * 创建人：赵海雷
      * 添加点赞
      *
      */
     public function addPraise(){
         
         $type = request()->post('type');
         $relation_id = request()->post('relation_id');
         $account = new VmemberAccount();
         $res = $account->addMemberDianzanRecord($type, $relation_id);
         
         return $res;
         
     }
    
}
