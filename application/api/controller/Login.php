<?php
/**
 * Login.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\api\controller;

use data\service\Member;
use think\Request;
/**
 * 前台用户登录
 *
 * @author Administrator
 *        
 */
class Login extends BaseController
{
    /**
     * 注册账户
     */
    public function register()
    {
        if (request()->isAjax()) {
            $bind_message_info = json_decode(Session::get("bind_message_info"), true);
            $member = new Member();
            $user_name = request()->post('username', '');
            $password = request()->post('password', '');
            $email = request()->post('email', '');
            $mobile = request()->post('mobile', '');
            $sendMobile = Session::get('sendMobile');
            if (empty($mobile)) {
                $retval = $member->registerMember($user_name, $password, $email, $mobile, '', '', '', '', '');
            } else {
                if ($sendMobile == $mobile) {
                    $retval = $member->registerMember($user_name, $password, $email, $mobile, '', '', '', '', '');
                } else 
                    if (empty($user_name)) {
                        $retval = $member->registerMember($user_name, $password, $email, $mobile, '', '', '', '', '');
                    }
            }
            if ($retval > 0) {
                // 微信的会员绑定
                if (empty($user_name)) {
                    $user_name = $mobile;
                }
                $this->wchatBindMember($user_name, $password, $bind_message_info);
            }
            
            return AjaxReturn($retval);
        } else {
            $this->getWchatBindMemberInfo();
            $config = new WebConfig();
            $instanceid = 0;
            // 判断是否开启邮箱注册
            $reg_config_info = $config->getRegisterAndVisit($instanceid);
            $reg_config = json_decode($reg_config_info["value"], true);
            if ($reg_config["is_register"] != 1) {
                $this->error("抱歉,商城暂未开放注册!", __URL__ . "/login/index");
            }
            if (strpos($reg_config['register_info'], "plain") === false && strpos($reg_config['register_info'], "mobile") === false) {
                $this->error("抱歉,商城暂未开放注册!", __URL__ . "/login/index");
            }
            $this->assign("reg_config", $reg_config);
            // 登录配置
            $web_config = new WebConfig();
            $loginConfig = $web_config->getLoginConfig();
            
            $loginCount = 0;
            if ($loginConfig['wchat_login_config']['is_use'] == 1) {
                $loginCount ++;
            }
            if ($loginConfig['qq_login_config']['is_use'] == 1) {
                $loginCount ++;
            }
            
            $code_config = $web_config->getLoginVerifyCodeConfig($this->instance_id);
            $this->assign('code_config', $code_config["value"]);
            
            $this->assign("loginCount", $loginCount);
            $this->assign("loginConfig", $loginConfig);
            
            return view($this->style . '/Login/register');
        }
        
    }
    /**
     * 检测手机是否存在
     * @return unknown
     */
    public function mobile()
    {
        $title = "检测当前手机是否已经注册";
        $user_mobile = request()->request('mobile', '');
        if(empty($user_mobile))
        {
            return $this->outMessage($title, '','-50', "缺少必填参数mobile");
        }
        $member = new Member();
        $exist = $member->memberIsMobile($user_mobile);
        return $this->outMessage($title, $exist);
        
    }

    /**
     * 判断邮箱是否存在
     */
    public function email()
    {
        $title = "检测邮箱是否已经注册";   
        $user_email = request()->request('email', '');
      if(empty($user_email))
        {
            return $this->outMessage($title, '','-50', "缺少必填参数email");
        }
        $member = new Member();
        $exist = $member->memberIsEmail($user_email);
        return $this->outMessage($title, $exist);
      
    }
    /**
     * 登陆
     */
    public function login()
    {
        $title = "会员登录";
        $username = request()->request('username', '');
        $password = request()->request('password', '');
        if(empty($username))
        {
            return $this->outMessage($title, '','-50', "缺少必填参数username");
        }
      //处理信息
        $user = new Member();
        $res = array();
        $res['data'] = $user->login($username, $password); 
        //返回信息
        if($res['data'] == 1){
            $member_info = $user->getUserDetail();
            $res['uid'] = $member_info['uid'];
            $res['instance_id'] = $member_info['instance_id'];
            $res['expire'] = 3600;
            $encode = $this->niuEncrypt(json_encode($res),'', $res['expire']);
            return $this->outMessage($title, array('data'=>$res['data'],'uid'=>$res['uid'],'instance_id'=>$res['instance_id'], 'token' =>$encode,'expire' => $res['expire']));
        }else{
            return $this->outMessage($title, $res, '-50', '登录失败,请检验用户名称以及密码!');
        }  
    }

    /**
     * 发送注册短信验证码
     *
     * @return boolean
     */
    public function sendSmsRegisterCode()
    {
        $params['mobile'] = request()->post('mobile', '');
        $vertification = request()->post('vertification', '');
        
        $web_config = new WebConfig();
        $code_config = $web_config->getLoginVerifyCodeConfig($this->instance_id);
        
        if ($code_config["value"]['pc'] == 1 && ! captcha_check($vertification)) {
            $result = [
                'code' => - 1,
                'message' => "验证码错误"
            ];
        } else {
            $params['shop_id'] = 0;
            $result = runhook('Notify', 'registBefor', $params);
            Session::set('mobileVerificationCode', $result['param']);
            Session::set('sendMobile', $params['mobile']);
        }
        
        if (! empty($result) && ! empty($result['param'])) {
            return $result = [
                'code' => 0,
                'message' => "发送成功"
            ];
        } else {
            return $result = [
                'code' => - 1,
                'message' => "发送失败"
            ];
        }
    }

    /**
     * 用户绑定手机号
     *
     * @return Ambigous <string, mixed>
     */
    public function sendSmsBindMobile()
    {
        $params['mobile'] = request()->post('mobile', '');
        $params['user_id'] = request()->post('user_id', '');
        $params['shop_id'] = 0;
        $result = runhook('Notify', 'bindMobile', $params);
        Session::set('mobileVerificationCode', $result['param']);
        Session::set('sendMobile', $params['mobile']);
        
        if (! empty($result) && ! empty($result['param'])) {
            return $result = [
                'code' => 0,
                'message' => "发送成功"
            ];
        } else {
            return $result = [
                'code' => - 1,
                'message' => "发送失败"
            ];
        }
    }
}