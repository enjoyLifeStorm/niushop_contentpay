<?php
/**
 * BaseController.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\api\controller;

\think\Loader::addNamespace('data', 'data/');
use think\Controller;
use think\Request;
use data\service\Member;


class BaseController extends Controller
{

    public $api_result;
    
    public $user;
    
    protected $shop_name;
    
    protected $uid;
    
    protected $instance_id;
    
    protected $auth_key = 'addexdfsdfewfscvsrdf!@#';
    /**
     * 当前版本的路径
     *
     * @var string
     */

    public function __construct()
    {
        parent::__construct();
        $this->api_result = new ApiResult();
        $this->checkToken();
        $member_service = new Member();
        $this->uid = $member_service->getSessionUid();
        
    }
    /**
     * 检测token
     */
    public function checkToken(){
        $token = request()->request("token", "");
        if(!empty($token))
        {
            $data = $this->niuDecrypt($token);
            $data = json_decode($data, true);
            if(!empty($data['uid']))
            {
                $member_service = new Member();
                $member_service->UidLogin($data['uid']);
            }
         
            
        }
    }

    /**
     * 返回信息
     * @param unknown $res
     * @return \think\response\Json
     */
    public function outMessage($title, $data, $code = 0, $message = "success"){

        $this->api_result->code = $code;
        $this->api_result->data = $data;
        $this->api_result->message = $message;
        $this->api_result->title = $title;
        
        if ($this->api_result) {
            return json($this->api_result);
        } else {
            abort(404);
        }
    }
    
    /**
     * 系统加密方法
     * @param string $data 要加密的字符串
     * @param string $key  加密密钥
     * @param int $expire  过期时间 单位 秒
     * @return string
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function niuEncrypt($data, $key = '', $expire = 0) {
        $key  = md5(empty($key) ? $this->auth_key : $key);
        $data = base64_encode($data);
        $x    = 0;
        $len  = strlen($data);
        $l    = strlen($key);
        $char = '';
    
        for ($i = 0; $i < $len; $i++) {
            if ($x == $l) $x = 0;
            $char .= substr($key, $x, 1);
            $x++;
        }
    
        $str = sprintf('%010d', $expire ? $expire + time():0);
    
        for ($i = 0; $i < $len; $i++) {
            $str .= chr(ord(substr($data, $i, 1)) + (ord(substr($char, $i, 1)))%256);
        }
        return str_replace(array('+','/','='),array('-','_',''),base64_encode($str));
    }
    
    /**
     * 系统解密方法
     * @param  string $data 要解密的字符串 （必须是think_encrypt方法加密的字符串）
     * @param  string $key  加密密钥
     * @return string
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function niuDecrypt($data, $key = ''){
        $key    = md5(empty($key) ? $this->auth_key : $key);
        $data   = str_replace(array('-','_'),array('+','/'),$data);
        $mod4   = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        $data   = base64_decode($data);
        $expire = substr($data,0,10);
        $data   = substr($data,10);
    
        if($expire > 0 && $expire < time()) {
            return '';
        }
        $x      = 0;
        $len    = strlen($data);
        $l      = strlen($key);
        $char   = $str = '';
    
        for ($i = 0; $i < $len; $i++) {
            if ($x == $l) $x = 0;
            $char .= substr($key, $x, 1);
            $x++;
        }
    
        for ($i = 0; $i < $len; $i++) {
            if (ord(substr($data, $i, 1))<ord(substr($char, $i, 1))) {
                $str .= chr((ord(substr($data, $i, 1)) + 256) - ord(substr($char, $i, 1)));
            }else{
                $str .= chr(ord(substr($data, $i, 1)) - ord(substr($char, $i, 1)));
            }
        }
        return base64_decode($str);
    }
}
    

class ApiResult
{

    public $code;

    public $message;

    public $data;
    
    public $title;

    public function __construct()
    {
        $this->code = 0;
        $this->title = '';
        $this->message = "success";
        $this->data = null;
    }

}