<?php
/**
 * Member.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */
namespace app\api\controller;

use data\service\Config;
use data\service\Member\MemberAccount as MemberAccount;
use data\service\Member as MemberService;
use data\service\NfxPromoter;
use data\service\NfxShopConfig;
use data\service\Order as OrderService;
use data\service\Platform;
use data\service\promotion\PromoteRewardRule;
use data\service\Promotion;
use data\service\Shop;
use data\service\UnifyPay;
use data\service\WebSite;
use data\service\Weixin;
use think\Request;
use think;
use think\Session;

/**
 * 会员
 *
 * @author Administrator
 *        
 */
class Member extends BaseController
{
    /**
     * 查询是否开启验证码
     * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
     */
    public function getLoginVerifyCodeConfig(){
        $title = "查询是否开启验证码";
        $web_config = new Config();
        $login_verify_code = $web_config->getLoginVerifyCodeConfig(0);
        return $this->outMessage($title, $login_verify_code);
    }
    /**
     * 查询是否开启通知
     * @return Ambigous <\think\response\Json, \think\Response, \think\response\View, \think\response\Xml, \think\response\Redirect, \think\response\Jsonp, unknown, \think\Response>
     */
    public function getNoticeConfig(){
        $title = "查询通知是否开启";
        $web_config = new Config();
        $noticeMobile = $web_config->getNoticeMobileConfig(0);
        $noticeEmail = $web_config->getNoticeEmailConfig(0);
        $notice['noticeEmail'] = $noticeEmail[0]['is_use'];
        $notice['noticeMobile'] = $noticeMobile[0]['is_use'];
        return $this->outMessage($title, $notice);
        
    }
    /**
     * 获取会员详细信息
     */
    public function getMemberDetail(){
        $title = "获取会员详细信息";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $member = new MemberService();
        $member_info = $member->getMemberDetail(0);
        return $this->outMessage($title, $member_info);
    }
    /**
     * 获取会员中心首页广告位
     */
    public function getMemberIndexAdv()
    {
        $title = "获取会员中心首页广告位";
        $platform = new Platform();
        $index_adv = $platform->getPlatformAdvPositionDetail(1152);
        return $this->outMessage($title, $index_adv);
    }
    

    /*
     * 单店B2C版
     */
    public function memberIndex()
    {
        $title = "会员个人中心数据";
        $member = new MemberService();
        $platform = new Platform();
        
        // 判断是否开启了签到送积分
        $config = new Config();
        $integralconfig = $config->getIntegralConfig($this->instance_id);
        $data['integralConfig'] = $integralconfig;
        
        // 判断用户是否签到
        $dataMember = new MemberService();
        $isSign = $dataMember->getIsMemberSign($this->uid, $this->instance_id);
        $data['isSign'] = $isSign;
        
        // 待支付订单数量
        $order = new OrderService();
        $unpaidOrder = $order->getOrderNumByOrderStatu([
            'order_status' => 0,
            "buyer_id" => $this->uid
        ]);
        $data['unpaidOrder'] = $unpaidOrder;
        
        // 待发货订单数量
        $shipmentPendingOrder = $order->getOrderNumByOrderStatu([
            'order_status' => 1,
            "buyer_id" => $this->uid
        ]);
        $data['shipmentPendingOrder'] = $shipmentPendingOrder;
        
        // 待收货订单数量
        $goodsNotReceivedOrder = $order->getOrderNumByOrderStatu([
            'order_status' => 2,
            "buyer_id" => $this->uid
        ]);
        $data['goodsNotReceivedOrder'] = $goodsNotReceivedOrder;
        
        // 退款订单
        $condition['order_status'] = array(
            'in', [-1, -2]
        );
        $condition['buyer_id'] = $this->uid;
        $refundOrder = $order->getOrderNumByOrderStatu($condition);
        $data['refundOrder'] = $refundOrder;
        
        return $this->outMessage($title, $data);
    }

    /**
     * 会员地址管理
     *
     * @return Ambigous <\think\response\View, \think\response\$this, \think\response\View>
     */
    public function getMemberAddressList()
    {
        $title = "获取会员地址";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $member = new MemberService();
        $addresslist = $member->getMemberExpressAddressList();
        return $this->outMessage($title, $addresslist);
    }

    /**
     * 添加地址
     *
     * @return Ambigous <multitype:unknown, multitype:unknown unknown string >
     */
    public function addMemberAddress()
    {
        $title = "添加会员地址,注意传入省市区id";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $member = new MemberService();
        $consigner = request()->request('consigner', '');
        $mobile = request()->request('mobile', '');
        if(empty($mobile))
        {
            return $this->outMessage($title, "",'-50', "缺少必填参数mobile");
        }
        $phone = request()->request('phone', '');
        $province = request()->request('province', '');
        if(empty($province))
        {
            return $this->outMessage($title, "",'-50', "缺少必填参数province");
        }
        $city = request()->request('city', '');
        if(empty($city))
        {
            return $this->outMessage($title, "",'-50', "缺少必填参数city");
        }
        $district = request()->request('district', '');
        $address = request()->request('address', '');
        if(empty($address))
        {
            return $this->outMessage($title, "",'-50', "缺少必填参数address");
        }
        $zip_code = request()->request('zip_code', '');
        $alias = request()->request('alias', '');
        $retval = $member->addMemberExpressAddress($consigner, $mobile, $phone, $province, $city, $district, $address, $zip_code, $alias);
        return $this->outMessage($title, $retval);
       
    }

    /**
     * 修改会员地址
     *
     * @return Ambigous <multitype:unknown, multitype:unknown unknown string >|Ambigous <\think\response\View, \think\response\$this, \think\response\View>
     */
    public function updateMemberAddress()
    {
       
        $title = "修改会员地址,注意传入省市区id";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $member = new MemberService();
        $id = request()->request('id', '');
        if(empty($id))
        {
            return $this->outMessage($title, "",'-50', "缺少必填参数id");
        }
        $consigner = request()->request('consigner', '');
        $mobile = request()->request('mobile', '');
        if(empty($mobile))
        {
            return $this->outMessage($title, "",'-50', "缺少必填参数mobile");
        }
        $phone = request()->request('phone', '');
        $province = request()->request('province', '');
        if(empty($province))
        {
            return $this->outMessage($title, "",'-50', "缺少必填参数province");
        }
        $city = request()->request('city', '');
        if(empty($city))
        {
            return $this->outMessage($title, "",'-50', "缺少必填参数city");
        }
        $district = request()->request('district', '');
        $address = request()->request('address', '');
        if(empty($address))
        {
            return $this->outMessage($title, "",'-50', "缺少必填参数address");
        }
        $zip_code = request()->request('zip_code', '');
        $alias = request()->request('alias', '');
        $retval = $member->updateMemberExpressAddress($id, $consigner, $mobile, $phone, $province, $city, $district, $address, $zip_code, $alias);
        return $this->outMessage($title, $retval);
    }

    /**
     * 获取用户地址详情
     *
     * @return Ambigous <\think\static, multitype:, \think\db\false, PDOStatement, string, \think\Model, \PDOStatement, \think\db\mixed, multitype:a r y s t i n g Q u e \ C l o , \think\db\Query, NULL>
     */
    public function getMemberAddressDetail()
    {
        $title = "获取用户地址详情";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $address_id = request()->request('id', 0);
        $member = new MemberService();
        $info = $member->getMemberExpressAddressDetail($address_id);
        return $this->outMessage($title, $info);
    }

    /**
     * 会员地址删除
     *
     * @return Ambigous <multitype:unknown, multitype:unknown unknown string >
     */
    public function memberAddressDelete()
    {
        $title = "删除会员地址";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $id = request()->request('id', '');
        $member = new MemberService();
        $res = $member->memberAddressDelete($id);
        return $this->outMessage($title, $res);
    
    }

    /**
     * 修改会员地址
     *
     * @return Ambigous <multitype:unknown, multitype:unknown unknown string >
     */
    public function updateAddressDefault()
    {
        $title = "修改默认会员地址";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $id = request()->request('id', '');
        $member = new MemberService();
        $res = $member->updateAddressDefault($id);
        return $this->outMessage($title, $res);
    }

    /**
     * 获取会员积分余额账户情况
     */
    public function getMemberAccount()
    {
        // 获取店铺的积分列表
        $title = "获取会员账户,分为平台账户和店铺会员账户";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $member = new MemberService();
        $account_list = $member->getShopAccountListByUser($this->uid, 1, 0);
        return $this->outMessage($title, $account_list);
    }

    /**
     * 会员账户流水
     */
    public function getMemberAccountRecordsList()
    {
        $title = "获取会员账户流水,分为平台账户和店铺会员账户,余额只有平台账户account_type:1积分2余额";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $shop_id = request()->request("shop_id", 0);
        $account_type = request()->request("account_type", 1);
        $condition['nmar.shop_id'] = $shop_id;
        $condition['nmar.uid'] = $this->uid;
        $condition['nmar.account_type'] = $account_type;
        // 查看用户在该商铺下的积分消费流水
        $member_point_list = $this->user->getAccountList(1, 0, $condition);
        return $this->outMessage($title, $member_point_list);
    }
    /**
     * 余额提现记录
     */
    public function balanceWithdraw()
    {
        $title = "获取会员提现记录";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        // 该店铺下的余额提现记录
        $member = new MemberService();
        $uid = $this->uid;
        $shopid = 0;
        $condition['uid'] = $uid;
        $condition['shop_id'] = $shopid;
        $withdraw_list = $member->getMemberBalanceWithdraw(1, 0, $condition);
        foreach ($withdraw_list['data'] as $k => $v) {
            if ($v['status'] == 1) {
                $withdraw_list['data'][$k]['status'] = '已同意';
            } else 
                if ($v['status'] == 0) {
                    $withdraw_list['data'][$k]['status'] = '已申请';
                } else {
                    $withdraw_list['data'][$k]['status'] = '已拒绝';
                }
        }
        return $this->outMessage($title, $withdraw_list);
        
    }

    /**
     * 会员优惠券
     */
    public function memberCoupon()
    {
        $title = "会员优惠券列表";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $member = new MemberService();
        $type = request()->post('type', '');
        $shop_id = $this->instance_id;
        $counpon_list = $member->getMemberCounponList($type, $shop_id);
        foreach ($counpon_list as $key => $item) {
            $counpon_list[$key]['start_time'] = date("Y-m-d", $item['start_time']);
            $counpon_list[$key]['end_time'] = date("Y-m-d", $item['end_time']);
        }
        return $this->outMessage($title, $counpon_list);
       
    }

    /**
     * 修改密码
     */
    public function modifyPassword()
    {
        $title = "会员修改密码";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $member = new MemberService();
        $uid = $this->uid;
        $old_password = request()->request('old_password', '');
        $new_password = request()->request('new_password', '');
        $retval = $member->ModifyUserPassword($uid, $old_password, $new_password);
          return $this->outMessage($title, $retval);
    }

    /**
     * 修改邮箱
     */
    public function modifyEmail()
    {
        $title = "会员修改邮箱";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $member = new MemberService();
        $uid = $this->uid;
        $email = request()->request('email', '');
        if(empty($email))
        {
            return $this->outMessage($title, "",'-50', "缺少必输信息email");
        }
        $retval = $member->modifyEmail($uid, $email);
        return $this->outMessage($title, $retval);
    }

    /**
     * 修改手机
     */
    public function modifyMobile()
    {
        $title = "会员修改手机";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $uid = $this->uid;
        $mobile = request()->request('mobile', '');
        if(empty($mobile))
        {
            return $this->outMessage($title, "",'-50', "缺少必输信息mobile");
        }
        $member = new MemberService();
        $retval = $member->modifyMobile($uid, $mobile);
          return $this->outMessage($title, $retval);
    }

    /**
     * 修改昵称
     *
     * @return unknown[]
     */
    public function modifyNickName()
    {
        $title = "会员修改昵称";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $uid = $this->uid;
        $nickname = request()->request('nickname', '');
        if(empty($nickname))
        {
            return $this->outMessage($title, "",'-50', "缺少必输信息nickname");
        }
        $member = new MemberService();
        $retval = $member->modifyNickName($uid, $nickname);
        return $this->outMessage($title, $retval);
    }
    /**
     * 积分兑换余额
     *
     * @return \think\response\View
     */
    public function ajaxIntegralExchangeBalance()
    {
        $title = "积分兑换余额";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $point = request()->request('amount', 0);
        $point = (float) $point;
        $shop_id = request()->post('shop_id', 0);
        $result = $this->user->memberPointToBalance($this->uid, $shop_id, $point);
        return $this->outMessage($title, $result);
    }

    /**
     * 账户列表
     * 任鹏强
     * 2017年3月13日10:52:59
     */
    public function accountList()
    {
        $title = "会员银行账户列表";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $member = new MemberService();
        $account_list = $member->getMemberBankAccount();
        return $this->outMessage($title, $account_list);
    }

    /**
     * 添加账户
     * 任鹏强
     * 2017年3月13日10:53:06
     */
    public function addAccount()
    {
        $title = "添加会员银行账户";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        
        $member = new MemberService();
        $uid = $this->uid;
        $realname = request()->request('realname', '');
        $mobile = request()->request('mobile', '');
        $bank_type = request()->request('bank_type', '1');
        $account_number = request()->request('account_number', '');
        $branch_bank_name = request()->request('branch_bank_name', '');
        $retval = $member->addMemberBankAccount($uid, $bank_type, $branch_bank_name, $realname, $account_number, $mobile);
        return $this->outMessage($title, $retval);
        
    }

    /**
     * 修改账户信息
     */
    public function updateAccount()
    {
        $title = "修改账户信息";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $member = new MemberService();
        $uid = $this->uid;
        $account_id = request()->request('id', '');
        $realname = request()->request('realname', '');
        $mobile = request()->request('mobile', '');
        $bank_type = request()->request('bank_type', '1');
        $account_number = request()->request('account_number', '');
        $branch_bank_name = request()->request('branch_bank_name', '');
        $retval = $member->updateMemberBankAccount($account_id, $branch_bank_name, $realname, $account_number, $mobile);
        return $this->outMessage($title, $retval);
    }

    /**
     * 删除账户信息
     */
    public function delAccount()
    {
        $title = "删除账户信息";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $member = new MemberService();
        $uid = $this->uid;
        $account_id = request()->request('id', '');
        $retval = $member->delMemberBankAccount($account_id);
        return $this->outMessage($title, $retval);
    }

    /**
     * 设置选中账户
     */
    public function checkAccount()
    {
        $title = "设置选中账户";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $member = new MemberService();
        $uid = $this->uid;
        $account_id = request()->request('id', '');
        $retval = $member->setMemberBankAccountDefault($uid, $account_id);
        return $this->outMessage($title, $retval);
    }
    // 用户签到
    public function signIn()
    {
        $title = "用户签到";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $rewardRule = new PromoteRewardRule();
        $retval = $rewardRule->memberSign($this->uid, $this->instance_id);
        return $this->outMessage($title, $retval);
    }
    // 分享送积分
    public function shareGivePoint()
    {
        $title = "分享送积分";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $rewardRule = new PromoteRewardRule();
        $retval = $rewardRule->memberShareSendPoint($this->instance_id, $this->uid);
        return $this->outMessage($title, $retval);
    }

    /**
     * 用户充值余额
     */
    public function recharge()
    {
        $title = "用户充值余额";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $pay = new UnifyPay();
        $pay_no = $pay->createOutTradeNo();
        return $this->outMessage($title, $pay_no);
    }

    /**
     * 创建充值订单
     */
    public function createRechargeOrder()
    {
        $title = "创建充值订单";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $recharge_money = request()->request('recharge_money', 0);
        if($recharge_money <= 0)
        {
            return $this->outMessage($title, "",'-50', "支付金额必须大于0");
        }
        $out_trade_no = request()->request('out_trade_no', '');
        if(empty($out_trade_no))
        {
            return $this->outMessage($title, "",'-50', "支付流水号不能为空");
        }
        $member = new MemberService();
        $retval = $member->createMemberRecharge($recharge_money, $this->uid, $out_trade_no);
        return $this->outMessage($title, $retval);
    }

    /**
     * 申请提现页面数据
     */
    public function toWithdraw()
    {
        $title = "申请提现页面数据";
        if(empty($this->uid))
        {
            return $this->outMessage($title, "",'-50', "无法获取会员登录信息");
        }
        $member = new MemberService();
        $account_list = $member->getMemberBankAccount(1);
        $data['account_list'] = $account_list;
        // 获取会员余额
        $uid = $this->uid;
        $shop_id = $this->shop_id;
        $members = new MemberAccount();
        $account = $members->getMemberBalance($uid);
        $instance_id = $this->instance_id;
        $this->assign('shop_id', $instance_id);
        $this->assign('account', $account);
        $config = new Config();
        $balanceConfig = $config->getBalanceWithdrawConfig($shop_id);
        // dump($balanceConfig);
        $cash = $balanceConfig['value']["withdraw_cash_min"];
        $this->assign('cash', $cash);
        $poundage = $balanceConfig['value']["withdraw_multiple"];
        $this->assign('poundage', $poundage);
        $withdraw_message = $balanceConfig['value']["withdraw_message"];
        $this->assign('withdraw_message', $withdraw_message);
        
        $this->assign('account_list', $account_list);
        return view($this->style . "/Member/toWithdraw");
        
        if (request()->isAjax()) {
            // 提现
            $uid = $this->uid;
            $withdraw_no = time() . rand(111, 999);
            $bank_account_id = request()->post('bank_account_id', '');
            $cash = request()->post('cash', '');
            $shop_id = $this->instance_id;
            $member = new MemberService();
            $retval = $member->addMemberBalanceWithdraw($shop_id, $withdraw_no, $uid, $bank_account_id, $cash);
            return AjaxReturn($retval);
        } else {
            
        }
    }

    /**
     * 绑定时发送短信验证码或邮件验证码
     *
     * @return number[]|string[]|string|mixed
     */
    function sendBindCode()
    {
        if (request()->isAjax()) {
            $params['email'] = request()->post('email', '');
            $params['mobile'] = request()->post('mobile', '');
            $params['user_id'] = $this->uid;
            $type = request()->post("type", '');
            $vertification = request()->post('vertification', '');
            if ($this->login_verify_code["value"]["pc"] == 1) {
                if (! captcha_check($vertification)) {
                    $result = [
                        'code' => - 1,
                        'message' => "验证码错误"
                    ];
                    return $result;
                } else {
                    $params['shop_id'] = 0;
                    if ($type == 'email') {
                        $hook = runhook('Notify', 'bindEmail', $params);
                        Session::set('VerificationCode', $hook['param']);
                    } elseif ($type == 'mobile') {
                        $hook = runhook('Notify', 'bindMobile', $params);
                        Session::set('VerificationCode', $hook['param']);
                    }
                    
                    if (! empty($hook) && ! empty($hook['param'])) {
                        
                        $result = [
                            'code' => 0,
                            'message' => '发送成功'
                        ];
                    } else {
                        
                        $result = [
                            'code' => - 1,
                            'message' => '发送失败'
                        ];
                    }
                }
                return $result;
            } else {
                $params['shop_id'] = 0;
                if ($type == 'email') {
                    $hook = runhook('Notify', 'bindEmail', $params);
                    Session::set('VerificationCode', $hook['param']);
                } elseif ($type == 'mobile') {
                    $hook = runhook('Notify', 'bindMobile', $params);
                    Session::set('VerificationCode', $hook['param']);
                }
                
                if (! empty($hook) && ! empty($hook['param'])) {
                    
                    $result = [
                        'code' => 0,
                        'message' => '发送成功'
                    ];
                } else {
                    
                    $result = [
                        'code' => - 1,
                        'message' => '发送失败'
                    ];
                }
                return $result;
            }
        }
    }

    /**
     * 检侧动态验证码是否输入正确
     */
    public function check_dynamic_code()
    {
        if (request()->isAjax()) {
            $code = request()->post("vertification", '');
            $verificationCode = Session::get("VerificationCode");
            if ($code != $verificationCode) {
                return $result = array(
                    "code" => - 1,
                    "message" => "动态验证码不一致"
                );
            }
        }
    }

    /**
     * 检测验证码是否正确
     */
    public function check_code()
    {
        if (request()->isAjax()) {
            $vertification = request()->post("vertification", '');
            if (! captcha_check($vertification)) {
                $result = [
                    'code' => - 1,
                    'message' => "验证码错误"
                ];
                return $result;
            }
        }
    }

    /**
     * 更改用户头像
     */
    public function modifyFace()
    {
        $member = new MemberService();
        if (Request()->isAjax()) {
            $user_headimg = request()->post('user_headimg', '');
            $res = $member->ModifyUserHeadimg($this->uid, $user_headimg);
            return AjaxReturn($res);
        } else {
            $member_info = $member->getMemberDetail();
            $member_img = $member_info['user_info']['user_headimg'];
            $this->assign("member_img", $member_img);
        }
        return view($this->style . "/Member/modifyFace");
    }
    
    /**
     * 我的收藏
     */
    public function myCollection(){
        if(request()->isAjax()){
            $member = new MemberService();
            $page = request()->post('page', '1');
            $type = request()->post('type', 0);
            $condition = array(
                "nmf.fav_type" => 'goods',
                "nmf.uid" => $this->uid
            );
            if($type == 1){//获取本周内收藏的商品
                $start_time = mktime(0, 0 , 0,date("m"),date("d")-date("w")+1,date("Y"));
                $end_time = mktime(23,59,59,date("m"),date("d")-date("w")+7,date("Y"));
                $condition["fav_time"] = array("between",$start_time.",".$end_time); 
            }else if($type == 2){//获取本月内收藏的商品
                $start_time = mktime( 0, 0, 0, date("m"),1,date("Y"));
                $end_time = mktime( 23, 59, 59, date("m"), date("t"), date("Y"));
                $condition["fav_time"] = array("between",$start_time.",".$end_time); 
            }else if($type == 3){//获取本年内收藏的商品
                $start_time = strtotime(date("Y",time())."-1"."-1");
                $end_time = strtotime(date("Y",time())."-12"."-31");
                $condition["fav_time"] = array("between",$start_time.",".$end_time); 
            }
            
            $goods_collection_list = $member->getMemberGoodsFavoritesList($page, PAGESIZE, $condition, "fav_time desc");
            foreach ($goods_collection_list['data'] as $k => $v){
                $v['fav_time'] = date("Y-m-d H:i:s",$v['fav_time']);
            }
            return $goods_collection_list;
        }
        return view($this->style . '/Member/myCollection');
    }
    
    /**
     * 添加收藏
     */
    public function FavoritesGoodsorshop(){
        if(request()->isAjax()){
            $fav_id = request()->post('fav_id', '');
            $fav_type = request()->post('fav_type', '');
            $log_msg = request()->post('log_msg', '');
            $member = new MemberService();
            $result = $member->addMemberFavouites($fav_id, $fav_type, $log_msg);
            return AjaxReturn($result);
        }
    }
    /**
     * 取消收藏
     */
    public function cancelFavorites()
    {
        if(request()->isAjax()){
            $fav_id = request()->post('fav_id', '');
            $fav_type = request()->post('fav_type', '');
            $member = new MemberService();
            $result = $member->deleteMemberFavorites($fav_id, $fav_type);
            return AjaxReturn($result);
        }
        
    }
}