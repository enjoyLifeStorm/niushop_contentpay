<?php
use think\Route;
use think\Cookie;
use think\Request;
use think\Log;
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
/*****************************************************************************************************设置后台登录模块**********************************************************************************/
//检测后台系统模块
//      if(ADMIN_MODULE != 'admin')
//     {
//         Route::group(ADMIN_MODULE,function(){
//             Route::rule(':controller/:action','admin/:controller/:action');
//             Route::rule(':controller','admin/:controller/index');
//             Route::rule('','admin/index/index');
          
//         });
//             Route::group('admin',function(){
//                 Route::rule(':controller/:action','workspc/:controller/:action');
//                 Route::rule(':controller','workspc/:controller/index');
//                 Route::rule('','workspc/index/index');
            
//             });
        
//     }
    
      
    
   /************************************************************************************************检测入口文件*****************************************************************************************/
    //检测入口文件
    $base_file = Request::instance()->baseFile();

    $alipay = strpos($base_file, 'alipay.php');
    $weixinpay = strpos($base_file, 'weixinpay.php');
    $is_api =  strpos($base_file, 'api.php');

    if($alipay)
    {
        Route::bind('wap/pay/aliUrlBack');
    }
    if($weixinpay)
    {
        Route::bind('wap/pay/wchatUrlBack');
    }
   
    /********************************************************************************检测打开端口******************************************************************************************************/
    //检测浏览器类型以及显示方式(电脑端、手机端)
    function getShowModule(){
    
        $default_client = Cookie::get('default_client');

        if(!empty($default_client)){
            $default_client = Cookie::get('default_client');
        }else{
            if(Request::instance()->get('default_client') == 'workspc'){
                $default_client = 'workspc';
            }else{
                $default_client = 'wap';
            }
        }
        $is_mobile = Request::instance()->isMobile();
    
        if($is_mobile)
        {
           return 'wap';
        }else{
            return 'workspc';
        }
    }
    $show_module = getShowModule();
    /*****************************************************************************************************针对商品详情设置路由***************************************************************************/
    //设置商品详情页面
    
  /*     if($show_module == 'workspc')
    {
        $goods_info_url = 'workspc/goods/goodsinfo';
        $workspc_url       = 'workspc/index/index';
    }else{
        $goods_info_url = 'wap/goods/goodsdetail';
        $workspc_url       = 'wap/index/index';
    }  */
    //pc端开启路由去除workspc
    /*****************************************************************************************************普通路由设置开始******************************************************************************/
    $common_route = [
        
        // 牛小课前台全部路由
        
        '[/]'     => [
        
            '/'         => ['workspc/index/index'],
        
        ],
        
        '[Article]'     => [
        
            
            ':action'         => ['workspc/Article/:action'],
            '/'               => ['workspc/Article/articleList'],
        
        ],
        
        '[article]'     => [
        
        
            ':action'         => ['workspc/article/:action'],
            '/'               => ['workspc/article/articleList'],
        ],
        
        '[Components]'     => [
        
        
            ':action'         => ['workspc/Components/:action'],
        
        ],
        
        '[components]'     => [
        
        
            ':action'         => ['workspc/components/:action'],
        
        ],
        
        '[Course]'     => [
        
        
            ':action'         => ['workspc/Course/:action'],
            '/'               => ['workspc/Course/courseList'],
        
        ],
        
        '[course]'     => [
        
        
            ':action'         => ['workspc/course/:action'],
            '/'               => ['workspc/course/courseList'],
        ],
        
        '[HelpCenter]'     => [
        
        
            ':action'         => ['workspc/Helpcenter/:action'],
        
        ],
        
        '[helpcenter]'     => [
        
        
            ':action'         => ['workspc/helpcenter/:action'],
        
        ],
        
        '[Index]'     => [
        
        
            ':action'         => ['workspc/Index/:action'],
            '/'               => ['workspc/Index/index'],
        ],
        
        '[index]'     => [
        
        
            ':action'         => ['workspc/index/:action'],
            '/'               => ['workspc/index/index'],
        
        ],
        
        '[Login]'     => [
        
        
            ':action'         => ['workspc/Login/:action'],
        
        ],
        
        '[login]'     => [
        
        
            ':action'         => ['workspc/login/:action'],
        
        ],
        
        '[Member]'     => [
        
        
            ':action'         => ['workspc/Member/:action'],
            '/'               => ['workspc/Member/index'],
        
        ],
        
        '[member]'     => [
        
        
            ':action'         => ['workspc/member/:action'],
            '/'               => ['workspc/member/index'],
        ],
        
        '[Order]'     => [
        
        
            ':action'         => ['workspc/Order/:action'],
        
        ],
        
        '[order]'     => [
        
        
            ':action'         => ['workspc/order/:action'],
        
        ],
        
        '[Package]'     => [
        
        
            ':action'         => ['workspc/Package/:action'],
        
        ],
        
        '[package]'     => [
        
        
            ':action'         => ['workspc/package/:action'],
        
        ],
        
        '[Teacher]'     => [
        
        
            ':action'         => ['workspc/Teacher/:action'],
        
        ],
        
        '[teacher]'     => [
        
        
            ':action'         => ['workspc/teacher/:action'],
        
        ],
        
        '[Works]'     => [
        
        
            ':action'         => ['workspc/Works/:action'],
        
        ],
        
        '[works]'     => [
        
        
            ':action'         => ['workspc/works/:action'],
        
        ],
        
        '[Pay]'     => [
        
        
            ':action'         => ['workswap/Pay/:action'],
        
        ],
        
        '[pay]'     => [
        
        
            ':action'         => ['workswap/pay/:action'],
        
        ],
        
        // 商城前台路由
        
        '[goods]'     => [
    
            //商品列表
            //  'goodsinfo'     => [$goods_info_url],
            ':action'         => ['shop/goods/:action'],
    
        ],
        '[list]'     => [
    
            //商品列表
            '/'         => ['shop/goods/goodslist'],
    
        ],
        
        //牛小课后台 总路由与相关分路由
        
        '[works_admin]'     => [
        
            
            ':controller/:action'         => ['worksadmin/:controller/:action'],
            ':controller'                 => ['worksadmin/:controller/index'],
            '/'                           => ['worksadmin/index/index'],
        ],
        
        // 牛小课前后台部分公共路由(因为控制器在workswap)    
        '[Task]'     => [
        
        
            ':action'         => ['workswap/Task/:action'],
        
        ],
        '[task]'     => [
        
        
            ':action'         => ['workswap/task/:action'],
        
        ],
     
        '[Upload]'     => [
        
        
            ':action'         => ['workswap/Upload/:action'],
        
        ],
        
        '[upload]'     => [
        
        
            ':action'         => ['workswap/upload/:action'],
        
        ],
        
        
    ];
    $works_wap_route = [
        '[/]'     => [
        
            '/'         => ['workswap/index/index'],
        
        ],
        '[Article]'     => [
    

            ':action'         => ['workswap/Article/:action'],
    
        ],
        '[article]'     => [
        
        
            ':action'         => ['workswap/article/:action'],
        
        ],
        '[Course]'     => [
        
        
            ':action'         => ['workswap/Course/:action'],
        
        ],
        '[course]'     => [
        
        
            ':action'         => ['workswap/course/:action'],
        
        ],
        
        '[Goods]'     => [
        
        
            ':action'         => ['workswap/Goods/:action'],
        
        ],
        '[goods]'     => [
        
        
            ':action'         => ['workswap/goods/:action'],
        
        ],
        '[Index]'     => [
        
        
            ':action'         => ['workswap/Index/:action'],
        
        ],
        '[index]'     => [
        
        
            ':action'         => ['workswap/index/:action'],
        
        ],
        '[Login]'     => [
        
        
            ':action'         => ['workswap/Login/:action'],
        
        ],
        '[login]'     => [
        
        
            ':action'         => ['workswap/login/:action'],
        
        ],
        '[Member]'     => [
        
        
            ':action'         => ['workswap/Member/:action'],
        
        ],
        '[member]'     => [
        
        
            ':action'         => ['workswap/member/:action'],
        
        ],
        '[Order]'     => [
        
        
            ':action'         => ['workswap/Order/:action'],
        
        ],
        '[order]'     => [
        
        
            ':action'         => ['workswap/order/:action'],
        
        ],
        '[Package]'     => [
        
        
            ':action'         => ['workswap/Package/:action'],
        
        ],
        '[package]'     => [
        
        
            ':action'         => ['workswap/package/:action'],
        
        ],
        '[Pay]'     => [
        
        
            ':action'         => ['workswap/Pay/:action'],
        
        ],
        '[pay]'     => [
        
        
            ':action'         => ['workswap/pay/:action'],
        
        ],
        '[Task]'     => [
        
        
            ':action'         => ['workswap/Task/:action'],
        
        ],
        '[task]'     => [
        
        
            ':action'         => ['workswap/task/:action'],
        
        ],
        '[Teacher]'     => [
        
        
            ':action'         => ['workswap/Teacher/:action'],
        
        ],
        '[teacher]'     => [
        
        
            ':action'         => ['workswap/teacher/:action'],
        
        ],
        '[Upload]'     => [
        
        
            ':action'         => ['workswap/Upload/:action'],
        
        ],
        '[upload]'     => [
        
        
            ':action'         => ['workswap/upload/:action'],
        
        ],
        '[Works]'     => [
        
        
            ':action'         => ['workswap/Works/:action'],
        
        ],
        '[works]'     => [
        
        
            ':action'         => ['workswap/works/:action'],
        
        ],
        
        
    
    ];
    
    $api_route = [];
    /*****************************************************************************************************普通路由设置结束******************************************************************************/
    if($is_api)
    {
        return $api_route;
    }else{
        if($show_module == 'workspc')
        {
            return $common_route;
        }else{
            return $works_wap_route;
        }
       
    }
  
    
   

