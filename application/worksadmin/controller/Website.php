<?php
namespace app\worksadmin\controller;
use data\worksservice\Website as WebsiteService;
use data\worksservice\Course as CourseService;
/**
 * 网站设定控制器
 * @author lzw
 *
 */
class Website extends BaseController
{

    public function __construct(){
        parent::__construct();
        
    }
    
    public function index(){
    }
    
    /**
     * 网站首页板块设定
     */
    public function homePlate(){
        if (request()->isAjax()) {
            $page_index = request()->post("page_index", 1);
            $page_size = request()->post('page_size', PAGESIZE);
            $plate_title = request()->post('plate_title', '');
            $ap_dis = request()->post('ap_display', '');
            $condition="";
            $website = new WebsiteService();
            if ($ap_dis != "") {
                $condition['ap_display'] = $ap_dis;
            }
            if (! empty($plate_title)) {
                $condition["plate_title"] = array(
                    "like",
                    "%" . $plate_title . "%"
                );
            }
            $list = $website->getHomePlateList($condition);
            return $list;
        }
        return view($this->style . "Website/homePlate");
    }
    /**
     * 添加板块
     */
    public function addhomePlate(){
        $plate_id = request()->get('plate_id','');
        if (request()->isAjax()) {
            $advhomePlate_data = $_POST['advhomePlate_data'];
            $website = new WebsiteService();
            $res = $website->addUpdateHomePlate($advhomePlate_data);
            return AjaxReturn($res);
        }
        $this->assign('plate_id',$plate_id);
        return view($this->style.'Website/addhomePlate');
    }
    /**
     * 修改版块
     */
    public function updatehomePlate(){
        $website = new WebsiteService();
        if (request()->isAjax()) {
            $advposition_data = $_POST['advposition_data'];
            $res = $website->addUpdateHomePlate($advposition_data);
            return AjaxReturn($res);
        }
        $plate_id = request()->get('plate_id', '');
        if (! is_numeric($plate_id)) {
            $this->error('未获取到信息');
        }
        $info = $website->getHomePlate($plate_id);
        $this->assign('info', $info);
        $this->assign('plate_id',$plate_id);
        return view($this->style . "Website/addhomePlate");
    }
    /**
     * 删除版块可多个删除,分隔
     */
    public function ajaxDelHomePlate(){
    
        if(request()->isAjax()){
            $plate_ids = request()->post('plate_ids', '');
            $website = new WebsiteService();
            $res = $website->delHomePlate($plate_ids);
            return AjaxReturn($res);
        }
    }
    /**
     * 修改版块排序
     */
    public function ajaxWebsiteSort(){
        if(request()->isAjax()){
            $data = $_POST['home_data'];
            $website = new WebsiteService();
            $res = $website->addUpdateHomePlate($data);
            return AjaxReturn($res);
        }
    }
    /**
     * 版块管理
     */
    public function homePlateWork(){
        $plate_id = request()->get('plate_id', '');
        $this->assign('plate_id', $plate_id);
        $relation_type = request()->get('relation_type','');
        if($relation_type == 1){
            $title = '课程';
        }elseif($relation_type == 2){
            $title = '套餐';
        }else{
            $title = '作品';
        }
        $this->assign('title',$title);
        $this->assign('relation_type',$relation_type);
        if (request()->isAjax()) {
            $page_index = request()->post("page_index", 1);
            $page_size = request()->post("page_size", PAGESIZE);
            $search_text = request()->post('search_text', '');
            $plate_id = request()->post('plate_id', '');
            $relation_type = request()->post('relation_type','');
            $website = new WebsiteService();
            $list = $website->getHomePlateWorksList($plate_id);
            
            foreach($list as $k => $v){
                if($relation_type == 1){
                    $course = new CourseService();
                    $courseinfo = $course ->getCourseInfo($v['relation_id']);
                    $list[$k]['title'] = $courseinfo['course_title'];
                    $list[$k]['source_price'] = $courseinfo['source_price'];
                    $list[$k]['current_price'] = $courseinfo['current_price'];
                }else if($relation_type == 2){
                    $course = new CourseService();
                    $packageinfo = $course->getPackageInfo($v['relation_id']);
                    $list[$k]['title'] = $packageinfo['package_title'];
                    $list[$k]['source_price'] = $packageinfo['source_price'];
                    $list[$k]['current_price'] = $packageinfo['current_price'];
                }
            }
            return $list;
        }
        
        /* $website = new WebsiteService();
        $list = $website->getHomePlateWorksList(7);
        dump($list);
        exit(); */
        
        return view($this->style . "Website/homePlateWork");
    }
/**
 * 添加版块下作品
 */
    public function addPlateWork(){
        if(request()->isAjax()){
            $website_service = new WebsiteService();
            $relation_ids = request()->post('relation_ids', '');
            $plate_id = request()->post('plate_id','');
            $relation_ids = rtrim($relation_ids, ',');
            $retval = $website_service->addBatchHomePlateWorks($plate_id, $relation_ids);
            return AjaxReturn($retval);
        }
    }
    /**
     * 删除板块下作品
     */
    public function ajaxDelHomePlateWork(){
        if(request()->isAjax()){
            $pr_id = request()->post('pr_id', '');
            $website = new WebsiteService();
            $res = $website->delHomePlateWorks($pr_id);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 修改板块下作品排序
     */
    public function ajaxUpdatePlateWorkSort(){
        if(request()->isAjax()){
            $pr_id = request()->post('pr_id','');
            $sort = request()->post('sort','');
            $website = new WebsiteService();
            $res = $website->updateHomePlateWorksSort($pr_id, $sort);
            return AjaxReturn($res);
        }
    }
    
}