<?php
namespace app\worksadmin\controller;

use data\worksservice\Works as worksservice;
/**
 * 作品管理控制器
 * @author lzw
 *
 */
class Works extends BaseController
{

    public function __construct(){
        parent::__construct();
        
    }
    
    public function index(){
        
        if(request()->isAjax()){
            
            $page_index = request()->post('page_index', 1);
            $page_size = request()->post('page_size', PAGESIZE);
            
            $works_service = new worksservice();
            $db_class_list = $works_service->getDbClassList($page_index, $page_size, '', 'sort asc');
            return $db_class_list;
        }
        return view($this->style . "Works/index");
    }
    
    /**
     * 添加修改作品库分类
     * @return \think\response\View
     */
    public function addUpdateDbClass(){
        
        $class_id = request()->get('class_id', 0);
        $works_service = new worksservice();
        
        $db_class_info = $works_service->getDbClassInfo($class_id);
        $this->assign('db_class_info', $db_class_info);
        
        return view($this->style . "Works/addUpdateDbClass");
    }
    
    /**
     * ajax添加修改作品库分类
     */
    public function ajaxAddUpdateDbClass(){
        
        if(request()->isAjax()){
            
            $db_class_data = $_POST['db_class_data'];
            
            $works_service = new worksservice();
            $res = $works_service->addUpdateDbClass($db_class_data);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 作品列表
     */
    public function works(){
        
        $class_id = request()->get('class_id', '');
        $this->assign('class_id', $class_id);
        
        if(request()->isAjax()){
        
            $page_index = request()->post('page_index', 1);
            $page_size = request()->post('page_size', PAGESIZE);
            $class_id = request()->post('class_id', 1);
            $condition['class_id'] = $class_id;
            $keyword = request()->post('keyword', '');
            $condition['works_name'] = ['like',"%".$keyword."%"];
            
            $works_service = new worksservice();
            $works_list = $works_service->getDbWorksList($page_index, $page_size, $condition, 'sort asc');
            return $works_list;
        }
        return view($this->style . "Works/works");
    }
    
    /**
     * 添加修改作品
     * @return \think\response\View
     */
    public function addUpdateDbWorks(){
        
        $works_id = request()->get('works_id', '');
        $this->assign('works_assign', $_GET);
        
        $works_service = new worksservice();
        $db_works_info = $works_service->getDbWorksInfo($works_id);
        $this->assign('db_works_info', $db_works_info);
        
        return view($this->style . "Works/addUpdateDbWorks");
    }
    
    /**
     * ajax添加修改作品
     */
    public function ajaxAddUpdateWorks(){
        
        if(request()->isAjax()){
            
            $db_works_data = $_POST['db_works_data'];
            $works_service = new worksservice();
            $res = $works_service->addUpdateDbWorks($db_works_data);
            return AjaxReturn($res);;
        }
    }
    
    /**
     * 删除作品
     */
    public function ajaxDelWorks(){
        
        if(request()->isAjax()){
            
            $works_id = request()->post('works_id_array', '');
            $works_service = new worksservice();
            $res = $works_service->delDbWorks(0, ['works_id' => array('in',$works_id)]);
            return AjaxReturn($res);
        }
        
    }
    
    /**
     * 删除作品库
     * @return multitype:unknown
     */
    public function ajaxDelDbClass(){
        if(request()->isAjax()){
        
            $class_id = request()->post('class_id_array', '');
            $works_service = new worksservice();
            $res = $works_service->delDbClass(0, ['class_id' => array('in',$class_id)]);
            return AjaxReturn($res);
        }
    }
}