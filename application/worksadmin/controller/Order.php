<?php
namespace app\worksadmin\controller;
use data\worksservice\Order as OrderService;
/**
 * 订单
 * @author lzw
 *
 */
class Order extends BaseController
{

    public function __construct(){
        parent::__construct();
        
    }
    /**
     * 订单列表
     */
    public function orderList(){
        
        
        if(request()->isAjax()){
            
            $page_index = request()->post('page_index', 1);
            $page_size = request()->post('page_size', PAGESIZE);
            $order_status = request()->post('order_status', 'all');
            
            $start_date = request()->post('start_date', '');
            $end_date = request()->post('end_date', '');
            
            $order_no = request()->post('order_no', '');
            
            $condition = array();
            //订单状态筛选
            if($order_status != 'all'){
                
                $condition['order_status'] = $order_status;
            }
            
            //时间筛选
            if(!empty($start_date)){
                $condition['create_time'] = array('gt', $start_date);
            }
            if(!empty($end_date)){
                $condition['create_time'] = array('lt', $end_date);
            }
            if(!empty($start_date) && !empty($end_date)){
                $condition['create_time'] = array('between time', array($start_date, $end_date));
            }
            
            //订单编号筛选
            if(!empty($order_no)){
                $condition['order_no'] = array('eq', $order_no);
            }
            
            
            $vorder_service = new OrderService();
            $order_list = $vorder_service->getOrderList($page_index, $page_size, $condition, 'create_time desc');
            return $order_list;
        }
        
        $status = request()->get('status', 'all');
        $this->assign('order_status', $status);
        $child_menu_list = array(
            0 => array(
                'url' => "Order/orderList",
                'menu_name' => '全部',
                "active" => $status == 'all' ? 1 : 0
            ),
            1 => array(
                'url' => "Order/orderList?status=0",
                'menu_name' => '待支付',
                "active" => $status == '0' ? 1 : 0
            ),
            2 => array(
                'url' => "Order/orderList?status=2",
                'menu_name' => '已完成',
                "active" => $status == '2' ? 1 : 0
            ),
            3 => array(
                'url' => "Order/orderList?status=-1",
                'menu_name' => '已取消',
                "active" => $status == '-1' ? 1 : 0
            )
        );

        $this->assign('child_menu_list', $child_menu_list);
        
        return view($this->style . 'Order/orderList');
    }
    
    /**
     * 线下支付订单
     */
    public function ajaxLinePayLine(){
        
        if(request()->isAjax()){
            $out_trade_no = request()->post('out_trade_no', '');
            $vorder_service = new OrderService();
            $res = $vorder_service->orderLinePay($out_trade_no);
            return AjaxReturn($res);
        }
    }
    
    /**
     * 订单详情
     */
    public function orderDetail(){
        
        $v_order_id = request()->get('v_order_id','');
        $vorder_service = new OrderService();
        $vorder_info = $vorder_service->getOrderInfo($v_order_id);
        $this->assign('order', $vorder_info);
        
        $item_order_list = $vorder_service->getOrderItemList($v_order_id);
        $this->assign('item_order_list', $item_order_list);
        return view($this->style . 'Order/orderDetail');
    }

}